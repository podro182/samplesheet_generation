#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#########################################################################################
# Copyright (C) 2020 GENOMIC MEDICINE - RIGSHOSPITALET                                  #
#########################################################################################
'''
Script for creating an empty database.

Copyright (C) 2020 GENOMIC MEDICINE - RIGSHOSPITALET

Authors : Bioinformatics team of the Genomic Medicine Department @ RIGSHOSPITALET
- Savvas Kinalis <savvas.kinalis@regionh.dk>
- Mathias Husted Torp <mathias.husted.torp@regionh.dk>
- Filipe Garrett Vieira <filipe.garrett.vieira@regionh.dk>
- Christian Baudet <christian.baudet.01@regionh.dk>
'''

#########################################################################################
# Import section                                                                        #
#########################################################################################
import os
import sys
from datetime import datetime

from pathlib import Path
from argparse import ArgumentParser, Namespace

from samplesheet_generation import db, constants

#########################################################################################
# Section : Functions
#########################################################################################
# Parses the command line arguments
def parse_arguments() -> Namespace:
    '''
    Parses the command line arguments.

    This function parses the command line arguments.

    Return
    ------
    Namespace
        A Namesapace object containing the parsed elements.
    '''
    # Configuring the argument parser
    parser = ArgumentParser(description='SSG: Create an empty database.')
    parser.add_argument('-d',
                        dest='directory',
                        required=True,
                        action='store',
                        help='Path for the directory where the SSG database file is located.')
    parser.add_argument('-f',
                        dest='force',
                        required=False,
                        action='store_true',
                        help='Force: create the empty database over an existing one.')
    if len(sys.argv) == 1:
        parser.print_help(sys.stderr)
        sys.exit(1)
    return parser.parse_args()


#########################################################################################
def main() -> None:
    '''
    Main function that coordinates the work.
    '''
    # Reading command line arguments
    args = parse_arguments()

    # Getting the base directory (directory where this script is located)
    base_dir = Path(os.path.realpath(__file__)).parent

    # Creating the path for the database file
    db_path = Path(args.directory).joinpath(constants.DB_FILENAME).absolute()
    if db_path.exists():
        if args.force:
            timestamp = datetime.now().strftime('%Y%m%d-%H%M%S')
            newfile = Path(db_path.absolute().as_posix() + '-' + timestamp)
            print(f'Renaming {db_path} to {newfile}.')
            os.rename(db_path.as_posix(), newfile.as_posix())
        else:
            raise RuntimeError(f'The database file already exists. Use the argument -f to force.')

    # Getting a database connection
    db_conn = db.create_database_connection(raise_exception=False, db_path=db_path)

    # Executing the create empty database routine
    db.create_empty_database(db_conn, base_dir)

    # Inserting assays and indexes
    db.init_assays_and_indexes_tables(db_conn, base_dir)

    # Inserting pronounceable words
    db.init_word(db_conn, base_dir)

    # Populating the patient table
    # populate_patient.populate_patient(db_conn, base_dir)

#########################################################################################
if __name__ == "__main__":
    main()
