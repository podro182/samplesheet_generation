#!/usr/bin/env sh

if [ -z "${DATA_DIR}" -o -z "${APP_CONFIG_FILE}" -o -z "${WORKERS}" -o -z "${PORT}" ]; then
    echo "Incomplete enviroment configuration:"
    echo "DATA_DIR=${DATA_DIR}"
    echo "APP_CONFIG_FILE=${APP_CONFIG_FILE}"
    echo "WORKERS=${WORKERS}"
    echo "PORT=${PORT}"
    exit 1
fi

exec gunicorn --bind 0.0.0.0:${PORT} samplesheet_generation --workers ${WORKERS}
