#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#########################################################################################
# Copyright (C) 2020 GENOMIC MEDICINE - RIGSHOSPITALET                                  #
#########################################################################################
'''
Script for updating the assay table on the database.

Copyright (C) 2020 GENOMIC MEDICINE - RIGSHOSPITALET

Authors : Bioinformatics team of the Genomic Medicine Department @ RIGSHOSPITALET
- Savvas Kinalis <savvas.kinalis@regionh.dk>
- Mathias Husted Torp <mathias.husted.torp@regionh.dk>
- Filipe Garrett Vieira <filipe.garrett.vieira@regionh.dk>
- Christian Baudet <christian.baudet.01@regionh.dk>
'''

#########################################################################################
# Import section                                                                        #
#########################################################################################
import sys

from pathlib import Path
from argparse import ArgumentParser, Namespace
from samplesheet_generation import db, constants

#########################################################################################
# Section : Functions
#########################################################################################
# Parses the command line arguments
def parse_arguments() -> Namespace:
    '''
    Parses the command line arguments.

    This function parses the command line arguments.

    Return
    ------
    Namespace
        A Namesapace object containing the parsed elements.
    '''
    # Configuring the argument parser
    parser = ArgumentParser(description='SSG: Populate patient ID table with csv. '
                                        'The csv should have 3 columns and no header: '
                                        'word, cpr_no, (patient) name')
    parser.add_argument('-d',
                        dest='directory',
                        required=True,
                        action='store',
                        help='Path for the directory where the SSG database file is located.')
    parser.add_argument('-f', '--file', required=True,
                        type=Path,
                        help='Input csv file with the patient ids to populate the database.')

    if len(sys.argv) == 1:
        parser.print_help(sys.stderr)
        sys.exit(1)
    return parser.parse_args()


#########################################################################################
def main() -> None:
    '''
    Main function that coordinates the work.
    '''
    # Reading command line arguments
    args = parse_arguments()

    # Creating the path for the database file
    db_path = Path(args.directory).joinpath(constants.DB_FILENAME).absolute()

    # Getting a database connection
    db_conn = db.create_database_connection(raise_exception=True, db_path=db_path)

    # Executing the update routine
    db.init_patient(db_conn, args.file)

    # Commit changes
    db_conn.commit()


#########################################################################################
if __name__ == "__main__":
    main()
