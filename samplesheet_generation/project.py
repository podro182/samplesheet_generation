# -*- coding: utf-8 -*-

#########################################################################################
# Copyright (C) 2020 GENOMIC MEDICINE - RIGSHOSPITALET                                  #
#########################################################################################

'''
Module that provide the "backend" methods for the project creation/visualisation
pages.

Copyright (C) 2020 GENOMIC MEDICINE - RIGSHOSPITALET

Authors : Bioinformatics team of the Genomic Medicine Department @ RIGSHOSPITALET
- Savvas Kinalis <savvas.kinalis@regionh.dk>
- Mathias Husted Torp <mathias.husted.torp@regionh.dk>
- Filipe Garrett Vieira <filipe.garrett.vieira@regionh.dk>
- Christian Baudet <christian.baudet.01@regionh.dk>
'''

#########################################################################################
# Import section                                                                        #
#########################################################################################
from flask import (Blueprint, g, redirect, render_template, request, url_for)

from samplesheet_generation.auth import login_required
from samplesheet_generation.db import get_db, query, insert_update
from samplesheet_generation.forms.project import ProjectCreationForm, ProjectEditForm


#########################################################################################
# Definition of the blueprint object.
blueprint = Blueprint('project', __name__, url_prefix='/project') #pylint:disable=invalid-name
#########################################################################################


#########################################################################################
# Public routing functions section                                                      #
#########################################################################################

@blueprint.route('/')
@login_required
def index():
    '''
    Renders the project list page (list of all registered projects)
    '''
    db_conn = get_db()
    return render_template('project/index.html',
                           projects=query.get_project_list(db_conn))


@blueprint.route('/create', methods=('GET', 'POST'))
@login_required
def create():
    '''
    Renders and validates the create project form.
    '''

    form = ProjectCreationForm(request.form)

    if request.method == 'POST' and form.validate():
        db_conn = get_db()
        insert_update.insert_project(db_conn,
                                     g.user['id'],
                                     form.name.data,
                                     form.internal.data,
                                     form.has_glass_no.data,
                                     form.contact.data,
                                     form.contact_mail.data,
                                     form.description.data)
        db_conn.commit()
        return redirect(url_for('project.index'))

    return render_template('project/create.html', form=form)


@blueprint.route('/<name>/edit', methods=('GET', 'POST'))
@login_required
def edit(name):
    '''
    Renders and validates the update project information form.
    '''

    form = ProjectEditForm(request.form)

    if request.method == 'GET':
        form.default(name)

    if request.method == 'POST' and form.validate():
        db_conn = get_db()
        insert_update.update_project(db_conn,
                                     name,
                                     form.internal.data,
                                     form.has_glass_no.data,
                                     form.contact.data,
                                     form.contact_mail.data,
                                     form.description.data)
        db_conn.commit()
        return redirect(url_for('project.index'))

    return render_template('project/edit.html', form=form)
