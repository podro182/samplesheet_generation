# -*- coding: utf-8 -*-

#########################################################################################
# Copyright (C) 2020 GENOMIC MEDICINE - RIGSHOSPITALET                                  #
#########################################################################################

'''
Module that groups all constants of the application.

Copyright (C) 2020 GENOMIC MEDICINE - RIGSHOSPITALET

Authors : Bioinformatics team of the Genomic Medicine Department @ RIGSHOSPITALET
- Savvas Kinalis <savvas.kinalis@regionh.dk>
- Mathias Husted Torp <mathias.husted.torp@regionh.dk>
- Filipe Garrett Vieira <filipe.garrett.vieira@regionh.dk>
- Christian Baudet <christian.baudet.01@regionh.dk>
'''

#pylint:disable=line-too-long

#########################################################################################
# Import Section                                                                        #
#########################################################################################
from typing import List

#########################################################################################
APP_CONFIG_FILE_ENV_VARNAME: str = 'APP_CONFIG_FILE'
DATA_DIR_ENV_VARNAME: str = 'DATA_DIR'

CONFIG_DB_FILE: str = 'CONFIG_DB_FILE'
CONFIG_SHEETS_DIR: str = 'CONFIG_SHEETS_DIR'

DB_DIRNAME: str = 'db'
DB_FILENAME: str = 'samplesheet_generation.sqlite'
SHEETS_DIRNAME: str = 'sheets'

#########################################################################################
# Minimum length for the sample number
MIN_SAMPLE_NUMBER_LENGTH: int = 5


#########################################################################################
# Minimum length for the patient number
MIN_PATIENT_NUMBER_LENGTH: int = 5


#########################################################################################
# Some constants for the sample sheet file
IEMFILEVERSION = '4'
WORKFLOW = 'GenerateFASTQ'
APPLICATION = 'FASTQ Only'

SAMPLESHEET_NAME: str = 'SampleSheet.csv'
METADATA_NAME: str = 'Metadata.csv'

NGC_SAMPLESHEET_NAME: str = 'SampleSheet_NGC.csv'
NGC_METADATA_NAME: str = 'Metadata_NGC.csv'

DATA_ROW: str = '{lane},{s_id},,,{i7n},{i7b},{i5n},{i5b},,{description}'
DATA_HEADER_ROW: str = 'Lane,Sample_ID,Sample_Plate,Sample_Well,I7_Index_ID,index,I5_Index_ID,index2,Sample_Project,Description'

NGC_DATA_ROW: str = '{s_id},{s_id},,,,{i7n},{i7b},{i5n},{i5b},,'
NGC_DATA_HEADER_ROW: str = 'Sample_ID,Sample_Name,Sample_Plate,Sample_Well,Index_Plate_Well,I7_Index_ID,index,I5_Index_ID,index2,Sample_Project,Description'

NGC_METADATA_HEADER: str = 'Subject ID,Requisition ID,Pipeline ID,Sample ID,Sample Material,DIN,Sample Concentration,Extraction Protocol,Library Kit,Library ID,Protocol ID,Research'
NGC_METADATA_ROW: str = '{sub_id},{req_id},{pipe_id},{s_id},{s_mat},{din},{s_conc},{ext_prot},{lib_kit},{lib_id},{prot_id},{research}'

# Place holder for the seq info information inside of a sample sheet or metadata file
SEQINFOPLACEHOLDER: str = 'S3Q1NF0'

# Place holder for the flowcell information inside of a sample sheet or metadata file
FLOWCELLPLACEHOLDER: str = 'FL0WC3LL1D'
