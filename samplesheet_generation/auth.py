# -*- coding: utf-8 -*-

#########################################################################################
# Copyright (C) 2020 GENOMIC MEDICINE - RIGSHOSPITALET                                  #
#########################################################################################

'''
Module that provide the "backend" methods for the login/logout and user registration
pages.

Copyright (C) 2020 GENOMIC MEDICINE - RIGSHOSPITALET

Authors : Bioinformatics team of the Genomic Medicine Department @ RIGSHOSPITALET
- Savvas Kinalis <savvas.kinalis@regionh.dk>
- Mathias Husted Torp <mathias.husted.torp@regionh.dk>
- Filipe Garrett Vieira <filipe.garrett.vieira@regionh.dk>
- Christian Baudet <christian.baudet.01@regionh.dk>
'''

#########################################################################################
# Import section                                                                        #
#########################################################################################
import functools

from flask import (Blueprint, g, redirect, render_template, request, session, url_for)

from werkzeug.security import generate_password_hash

from samplesheet_generation.db import get_db, query, insert_update
from samplesheet_generation.forms.auth import RegistrationForm, LoginForm

#########################################################################################
# Definition of the blueprint object.
blueprint = Blueprint('auth', __name__, url_prefix='/auth') #pylint:disable=invalid-name
#########################################################################################


#########################################################################################
# Decorators section                                                                    #
#########################################################################################

def login_required(view):
    '''
    This is a decorator that determines the views that requires a valid session (user
    logged in).
    '''
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if g.user is None:
            return redirect(url_for('auth.login'))
        return view(**kwargs)
    return wrapped_view

#########################################################################################
# Public routing functions section                                                      #
#########################################################################################

@blueprint.before_app_request
def load_logged_in_user():
    '''
    If a user id is stored in the session, load the user object from
    the database into ``g.user``.
    '''
    user_id = session.get('user_id')
    if user_id is None:
        g.user = None
    else:
        db_conn = get_db()
        g.user = query.get_user_by_id(db_conn, user_id)


@blueprint.route('/register', methods=('GET', 'POST'))
def register():
    '''
    Register a new user.

    Validates that the username is not already taken. Hashes the
    password for security.
    '''
    form = RegistrationForm(request.form)
    if request.method == 'POST' and form.validate():
        username = form.username.data
        password = form.password.data

        db_conn = get_db()
        insert_update.insert_user(db_conn, username, generate_password_hash(password))
        db_conn.commit()

        # store the user id in a new session and return to the index
        userdata = query.get_user_by_username(db_conn, username)
        session.clear()
        session['user_id'] = userdata['id']
        return redirect(url_for('samplesheet.index'))

    return render_template('auth/register.html', form=form)


@blueprint.route('/login', methods=('GET', 'POST'))
def login():
    '''
    Log in a registered user by adding the user id to the session.
    '''
    form = LoginForm(request.form)
    if request.method == 'POST' and form.validate():
        # store the user id in a new session and return to the index
        db_conn = get_db()
        userdata = query.get_user_by_username(db_conn, form.username.data)
        session.clear()
        session['user_id'] = userdata['id']
        return redirect(url_for('samplesheet.index'))

    return render_template('auth/login.html', form=form)


@blueprint.route('/logout')
def logout():
    '''
    Clear the current session, including the stored user id.
    '''
    session.clear()
    return redirect(url_for('project.index'))
