# -*- coding: utf-8 -*-

#pylint:disable=line-too-long

#########################################################################################
# Copyright (C) 2020 GENOMIC MEDICINE - RIGSHOSPITALET                                  #
#########################################################################################

'''
Module that provides functions for create the CSV/TSV files for sample sheets and
metadata files.

Copyright (C) 2020 GENOMIC MEDICINE - RIGSHOSPITALET

Authors : Bioinformatics team of the Genomic Medicine Department @ RIGSHOSPITALET
- Savvas Kinalis <savvas.kinalis@regionh.dk>
- Mathias Husted Torp <mathias.husted.torp@regionh.dk>
- Filipe Garrett Vieira <filipe.garrett.vieira@regionh.dk>
- Christian Baudet <christian.baudet.01@regionh.dk>
'''

#########################################################################################
# Import section                                                                        #
#########################################################################################
import os

from re import match
from pathlib import Path
from datetime import datetime

from typing import List, Mapping, Tuple

from samplesheet_generation.db import query
from samplesheet_generation import constants as const

#########################################################################################
# Public functions section                                                              #
#########################################################################################

def create_samplesheet_file(db_conn,
                            samplesheet_id: int,
                            config: Mapping[str, str]) -> None:
    '''
    Method that creates the sample sheet file accordingly with the old naming schema.

    Argument
    --------
    db_conn
        The database connection.
    samplesheet_id: int
        The sample sheet database identifier.
    config: Mapping[str, str]
        The dictionary that contains the configuration information (Path for the
        sheets directory defined by the key const.CONFIG_SHEETS_DIR).
    '''

    # Retrieving information from the database
    samplesheet_info = query.get_samplesheet_by_id(db_conn, samplesheet_id)
    samples = query.get_samples_by_samplesheet_id(db_conn, samplesheet_id)
    ngc_samples = query.get_ngc_samples_by_samplesheet_id(db_conn, samplesheet_id)

    # Creating the output folder
    destination_folder = __create_output_folder(samplesheet_info['fullname'], config)

    # Creating the sample sheet for our data_organisation pipeline
    __create_samplesheet(db_conn, samplesheet_info, samples, destination_folder)

    # Creating the sample sheet and metadata files for NGC
    if ngc_samples:
        __create_ngc_files(db_conn, samplesheet_info, ngc_samples, destination_folder)


#########################################################################################
# Private functions section                                                             #
#########################################################################################
def __create_output_folder(fullname: str, config: Mapping[str, str]) -> Path:
    '''
    Creates the path for the output folder with the given sample sheet full name.

    Arguments
    ---------
    fullname: str
        The sample sheet full name.
    config: Mapping[str, str]
        The dictionary that contains the configuration information (Path for the
        sheets directory defined by the key const.CONFIG_SHEETS_DIR).

    Return
    ------
    Path
        A Path object that represents the output folder.
    '''
    if not const.CONFIG_SHEETS_DIR in config:
        raise RuntimeError(f'Incomplete configuration. Missing sample sheets directory path.')
    if not config[const.CONFIG_SHEETS_DIR]:
        raise RuntimeError(f'Incomplete configuration. Missing sample sheets directory path.')
    samplesheet_folder = Path(config[const.CONFIG_SHEETS_DIR])
    destination_folder = samplesheet_folder.joinpath(fullname)
    if not destination_folder.exists():
        destination_folder.mkdir(parents=True, exist_ok=True)
    return destination_folder


def __create_samplesheet(db_conn,
                         samplesheet_info: Mapping[str, object],
                         samples: List[Mapping[str, object]],
                         destination_folder: Path) -> None:
    '''
    Create the sample sheet for our data_organisation pipeline.

    Arguments
    ---------
    db_conn
        The database connection object.
    samplesheet_info: Mapping[str, object]
        A dictionary containing the sample sheet data.
    samples: List[Mapping[str, object]]
        The list of samples to be included into the sample sheet file.
    destination_folder: Path
        The path for the folder where the files is going to
        be written.
    '''
    # Creating a buffer with the initial content of the sample sheet file
    buffer = __create_samplesheet_preamble(samplesheet_info, False)

    # Adding the the sample data to the buffer
    buffer.extend(__create_samplesheet_data_section(db_conn,
                                                    samplesheet_info,
                                                    samples))

    # Finally, writing the file.
    output_file = destination_folder.joinpath(const.SAMPLESHEET_NAME)
    __write_file(output_file, buffer)


def __create_ngc_files(db_conn,
                       samplesheet_info: Mapping[str, object],
                       ngc_samples: List[Mapping[str, object]],
                       destination_folder: Path) -> None:
    '''
    Create the sample sheet and metadata files for NGC.

    Arguments
    ---------
    db_conn
        The database connection object.
    samplesheet_info: Mapping[str, object]
        A dictionary containing the sample sheet data.
    ngc_samples: List[Mapping[str, object]]
        The list of NGC samples to be included into files.
    destination_folder: Path
        The path for the folder where the files is going to
        be written.
    '''
    # Creating a buffer with the initial content of the sample sheet file
    buffer = __create_samplesheet_preamble(samplesheet_info, True)

    # Creating sample sheet and metadata rows
    (datarows, metadatarows) = __create_ngc_samplesheet_data_section(db_conn,
                                                                     samplesheet_info,
                                                                     ngc_samples)

    # Adding the the sample data to the buffer
    buffer.extend(datarows)

    # Writing the sample sheet file.
    output_ss_file = destination_folder.joinpath(const.NGC_SAMPLESHEET_NAME)
    __write_file(output_ss_file, buffer, eol='\n')

    # Writing the metadata file.
    output_meta_file = destination_folder.joinpath(const.NGC_METADATA_NAME)
    __write_file(output_meta_file, metadatarows, eol='\n')


def __create_samplesheet_preamble(samplesheet_info: Mapping[str, object],
                                  ngc: bool) -> List[str]:
    '''
    Auxiliary function that creates the sample sheet preamble (sections
    Header, Reads, and Settings).

    Argument
    --------
    samplesheet_info: Mapping[str, object]
        A dictionary containing the sample sheet data.
    ngc: bool
        If True, the preamble is created for the NGC sample sheet.

    Return
    ------
    List[str]
        A list of strings that contains the preamble lines.
    '''
    buffer = __create_samplesheet_header_section(samplesheet_info, ngc)
    buffer.extend(__create_samplesheet_reads_section(samplesheet_info))
    # For now, only NGC Sample Sheets won't include adapters
    buffer.extend(__create_samplesheet_settings_section(samplesheet_info, add_adapter=not ngc))
    return buffer


def __create_samplesheet_header_section(samplesheet_info: Mapping[str, object],
                                        ngc: bool) -> List[str]:
    '''
    Auxiliary function that creates the sample sheet Header section.

    Argument
    --------
    samplesheet_info: Mapping[str, object]
        A dictionary containing the sample sheet data.
    ngc: bool
        If True, the header section is created for the NGC sample sheet.

    Return
    ------
    List[str]
        A list of strings that contains the Header section lines.
    '''
    buffer = ['[Header]']
    buffer.append(f'IEMFileVersion,{samplesheet_info["iemfileversion"]}')
    buffer.append(f'Date,{samplesheet_info["date"]}')
    buffer.append(f'Workflow,{samplesheet_info["workflow"]}')
    buffer.append(f'Application,{samplesheet_info["application"]}')
    buffer.append(f'Assay,{samplesheet_info["assay"]}')
    if ngc:
        buffer.append(f'Description,NGC')
    else:
        buffer.append(f'Description,{samplesheet_info["description"]}')
    buffer.append(f'Chemistry,{samplesheet_info["chemistry"]}')
    buffer.append('')
    return buffer


def __create_samplesheet_reads_section(samplesheet_info: Mapping[str, object]) -> List[str]:
    '''
    Auxiliary function that creates the sample sheet Reads section.

    Argument
    --------
    samplesheet_info: Mapping[str, object]
        A dictionary containing the sample sheet data.

    Return
    ------
    List[str]
        A list of strings that contains the Reads section lines.
    '''
    buffer = ['[Reads]']
    buffer.append(f'{samplesheet_info["reads1"]}')
    buffer.append(f'{samplesheet_info["reads2"]}')
    buffer.append('')
    return buffer


def __create_samplesheet_settings_section(samplesheet_info: Mapping[str, object], add_adapter: bool=True) -> List[str]:
    '''
    Auxiliary function that creates the sample sheet Settings section.

    Argument
    --------
    samplesheet_info: Mapping[str, object]
        A dictionary containing the sample sheet data.

    Return
    ------
    List[str]
        A list of strings that contains the Settings section lines.
    '''
    buffer = ['[Settings]']
    buffer.append('ReverseComplement,0')
    if add_adapter:
        buffer.append(f'Adapter,{samplesheet_info["adapter1"]}')
        if samplesheet_info['adapter2']:
            buffer.append(f'AdapterRead2,{samplesheet_info["adapter2"]}')

    if samplesheet_info['umi']:
        for read in [1, 2]:
            buffer.append(f'Read{read}UMILength,{samplesheet_info["umi"]}')
        for read in [1, 2]:
            buffer.append(f'Read{read}StartFromCycle,{int(samplesheet_info["umi"]) + 2}')

    buffer.append('')
    return buffer


def __generate_content_string(buffer: List[str], eol: str = '\r\n') -> str:
    '''
    Auxiliary function that replaces some unwanted characters from the sample sheet string.

    Argument
    --------
    buffer: List[str]
        The buffer that contains the sample sheet content.
    eol: str
        The end of line delimiter that will be used to join the lines.

    Return
    ------
    str
        The string that contains the cleaned sample sheet content.
    '''
    mapping = str.maketrans({
        'Æ': 'AE',
        'Ø': 'OE',
        'Å': 'AA',
        'æ': 'ae',
        'ø': 'oe',
        'å': 'aa',
    })
    string = eol.join(buffer)
    return string.translate(mapping).encode()


def __create_samplesheet_data_section_header(ngc: bool) -> List[str]:
    '''
    Auxiliary function that creates the sample sheet data section header.

    Argument
    --------
    ngc: bool
        If True, returns the data section header for the NGC sample sheet.

    Return
    ------
    List[str]
        A list with 2 strings: the section title and the column names row.
    '''
    buffer = ['[Data]']
    buffer.append(const.NGC_DATA_HEADER_ROW if ngc else const.DATA_HEADER_ROW)
    return buffer


def __create_samplesheet_data_section(db_conn,
                                      samplesheet_info: Mapping[str, object],
                                      samples: List[Mapping[str, object]]) -> List[str]:
    '''
    Auxiliary function that creates the sample sheet data section content.

    Arguments
    ---------
    db_conn
        The database connection object.
    samplesheet_info: Mapping[str, object]
        A dictionary containing the sample sheet attributes.
    samples: List[Mapping[str, object]],
        A list of dictionaries containing sample attributes.

    Return
    ------
    List[str]
        A list with 2 strings: the section title and the column names row
    '''

    # Retrieving the indexes dictionary
    index_dict = __get_index_dict(db_conn,
                                  samplesheet_info['assay'],
                                  samplesheet_info['sequencer'],
                                  samplesheet_info['reagentkit'])

    # Creating the buffer with the first lines of the data section header
    buffer = __create_samplesheet_data_section_header(False)

    # Adding the lines of each sample
    for sample in samples:
        buffer.append(__create_samplesheet_data_row(db_conn,
                                                    samplesheet_info,
                                                    index_dict,
                                                    sample))

    buffer.append('')

    return buffer


def __create_ngc_samplesheet_data_section(db_conn,
                                          samplesheet_info: Mapping[str, object],
                                          ngc_samples: List[Mapping[str, object]]) -> Tuple[List[str], List[str]]:
    '''
    Auxiliary function that creates the NGC sample sheet data section content.

    Arguments
    ---------
    db_conn
        The database connection object.
    samplesheet_info: Mapping[str, object]
        A dictionary containing the sample sheet attributes.
    ngc_samples: List[Mapping[str, object]],
        A list of dictionaries containing NGC sample attributes.

    Return
    ------
    Tuple[List[str], List[str]]
        A 2-tuple with 2 list of strings: data rows and metadata rows.
    '''

    # Retrieving the indexes dictionary
    index_dict = __get_index_dict(db_conn,
                                  samplesheet_info['assay'],
                                  samplesheet_info['sequencer'],
                                  samplesheet_info['reagentkit'])

    # Creating the buffer with the first lines of the data section header
    buffer = __create_samplesheet_data_section_header(True)
    metadata = [const.NGC_METADATA_HEADER]

    # Adding the lines of each sample
    for sample in ngc_samples:
        (datarow, metadatarow) = __create_ngc_data_rows(db_conn,
                                                        samplesheet_info,
                                                        index_dict,
                                                        sample)
        buffer.append(datarow)
        metadata.append(metadatarow)

    buffer.append('')
    metadata.append('')

    return (buffer, metadata)


def __get_index_dict(db_handler,
                     assay_name: str,
                     sequencer: str,
                     reagentkit_version: str) -> Mapping[str, object]:
    '''
    Auxiliary function that returns the list of indexes accordingly with the
    given assay and sequencer.

    Arguments
    ---------
    db_handler
        The database connection handler
    assay_name: str
        The assay name.
    sequencer: str
        The sequence machine code.
    reagentkit_version: str
        The reagent kit version.

    Return
    ------
    Mapping[str, object]
        A dictionary containing the following elements:
        - i7 : dictionary of i7 indexes
        - i5 : dictionary of i5 indexes
        - uniqueDI : boolean that indicates if the assay is uniqueDI.
    '''

    assay_info = query.get_assay_by_name(db_handler, assay_name)

    machine = query.get_sequencing_machine_by_code(db_handler, sequencer)
    novaseqbases = query.get_novaseqbases_by_model_reagentkit(
        db_handler, machine['model'], reagentkit_version)

    index2 = {'' : ''}
    if assay_info['singleIndex']:
        index1 = query.get_index_set(db_handler, assay_info['id'],
                                     'singleIndex', indexnr=1)
    elif assay_info['doubleIndex']:
        index1 = query.get_index_set(db_handler, assay_info['id'],
                                     'doubleIndex', indexnr=1)
        index2 = query.get_index_set(db_handler, assay_info['id'],
                                     'doubleIndex', indexnr=2,
                                     novaseqbases=novaseqbases)
    elif assay_info['uniqueDI']:
        index1 = query.get_index_set(db_handler, assay_info['id'],
                                     'uniqueDI', indexnr=1)
        index2 = query.get_index_set(db_handler, assay_info['id'],
                                     'uniqueDI', indexnr=2,
                                     novaseqbases=novaseqbases)

    return {'i7': index1, 'i5': index2, 'uniqueDI': assay_info['uniqueDI']}


def __get_sample_indexes(index_dict, index1: str, index2: str):
    '''
    Auxiliary function that get the correct index sequence.

    Arguments
    ---------
    index_dict: dict
        The dictionary of indexes.
    index1: str
        The index1 name (or DNA sequence)
    index2: str
        The index2 name (or DNA sequence)

    Return
    ------
    tuple
        A 4-element tuple with the following information:
        - i7name : the name of the i7 index
        - i5name : the name of the i5 index
        - i7bases : the DNA sequence of the i7 index
        - i5bases : the DNA sequence of the i5 index
    '''
    if match(r'^[ATCG]+$', index1) and match(r'^[ATCG]*$', index2):
        return ('', '', index1, index2)

    i7name = index1
    i7bases = index_dict['i7'][index1]
    i5name = index1 if index_dict['uniqueDI'] else index2
    i5bases = index_dict['i5'][index1] if index_dict['uniqueDI'] else index_dict['i5'][index2]

    return (i7name, i5name, i7bases, i5bases)


def __create_samplesheet_data_row(db_conn,
                                  samplesheet_info: Mapping[str, object],
                                  index_dict: Mapping[str, object],
                                  sample: Mapping[str, object]) -> str:
    '''
    Returns a sample sheet data row string for the given sample.

    Arguments
    ---------
    db_conn
        The database connection object.
    samplesheet_info: Mapping[str, object]
        A dictionary containing the sample sheet attributes.
    index_dict: Mapping[str, object]
        The dictionary of barcode indexes.
    sample: Mapping[str, object],
        A dictionary containing the sample attributes.

    Return
    ------
    str
        The data section row that is associated with the given sample.
    '''
    project_info = query.get_project_by_id(db_conn, sample['project_id'])

    assay_info = query.get_assay_by_name(db_conn, samplesheet_info['assay'])

    sample_name = __create_sample_name(db_conn,
                                       sample,
                                       project_info,
                                       assay_info['name_shortcut'])

    (i7n, i5n, i7b, i5b) = __get_sample_indexes(index_dict,
                                                sample['index1'],
                                                sample['index2'])

    return const.DATA_ROW.format(lane=sample['lane'],
                                 s_id=sample_name,
                                 i7n=i7n, i7b=i7b,
                                 i5n=i5n, i5b=i5b,
                                 description=sample['description'])


def __create_ngc_data_rows(db_conn, #pylint:disable=too-many-locals
                           samplesheet_info: Mapping[str, object],
                           index_dict: Mapping[str, object],
                           sample: Mapping[str, object]) -> Tuple[str, str]:
    '''
    Returns a 2-tuple containing the sample sheet data row string and the metadata
    row string for the given sample.

    Arguments
    ---------
    db_conn
        The database connection object.
    samplesheet_info: Mapping[str, object]
        A dictionary containing the sample sheet attributes.
    index_dict: Mapping[str, object]
        The dictionary of barcode indexes.
    sample: Mapping[str, object],
        A dictionary containing the sample attributes.

    Return
    ------
    str
        The data section row that is associated with the given sample.
    '''
    # Retrieving database information
    assay_info = query.get_assay_by_name(db_conn, samplesheet_info['assay'])
    library = query.get_active_library_by_name(db_conn, sample['library'])
    library_number = query.get_sample_library_number(db_conn, sample['id'])
    sample_material = query.get_sample_material_by_name(db_conn, sample['type'])

    # Creating sample data and metadata components
    external_ids = sample['external_ids'].split(';')

    subject_id = external_ids[1].strip()
    requisition_id = external_ids[0].strip()
    sample_id = sample['sample_number']
    material = sample_material['ngc_name']
    library_id = __format_library_id(library['name'], library_number)
    protocol_id = '_'.join([library['name'], library['version']])

    sample_name = f'{subject_id}-{sample_id}-{material}-{protocol_id}-{const.FLOWCELLPLACEHOLDER}-{library_id}'

    (i7n, i5n, i7b, i5b) = __get_sample_indexes(index_dict,
                                                sample['index1'],
                                                sample['index2'])

    return (const.NGC_DATA_ROW.format(s_id=sample_name,
                                      i7n=i7n, i7b=i7b,
                                      i5n=i5n, i5b=i5b),
            const.NGC_METADATA_ROW.format(sub_id=subject_id,
                                          req_id=requisition_id,
                                          pipe_id=const.NGC_GERMLINE if library['name'] == 'WGS' else const.NGC_IDSNP,
                                          s_id=sample_id,
                                          s_mat=material,
                                          din='',
                                          s_conc='',
                                          ext_prot='',
                                          lib_kit=assay_info['ngc_name'],
                                          lib_id=library_id,
                                          prot_id=protocol_id,
                                          research='No'))


def __create_sample_name(db_conn,
                         sample: Mapping[str, object],
                         project_info: Mapping[str, object],
                         assay_shortcut: str) -> str:
    '''
    Auxiliary function that creates the sample name for the sample sheet
    following the old naming schema.

    Arguments
    ---------
    db_conn
        The database connection object.
    sample: Mapping[str, object]
        The dictionary that contains the sample attributes.
    project_info: Mapping[str, object]
        The dictionary that contains the project attributes.
    assay_shortcut: str
        The assay shortcut name.

    Return
    ------
    str
        The sample name accordingly with the new naming schema
    '''
    # The sample name schema is composed by the following structure:
    # - patient block
    #   + hashed CPR (patient ID) if possible
    # - sample block
    #   + this is the glass number (sample number)
    # - tissue block
    #   + type
    #   + origin
    #   + preservation
    # - library block
    #   + library type
    #   + library version
    #   + library kit
    #   + library id
    # - seqinfo block (here we put the S3Q1NF0 placeholder - information is replaced later)
    #   + sequencing run date
    #   + sequencing run machine code
    #   + sequencing run flowcell
    # - project block
    #   + project owner
    #   + project LABKA code (only LABKA for now)
    #   + name of the project
    # - custom block
    #   + extra information (like pool number for instance)
    #   + concatenation of project name and patient number
    # The blocks are separated by dashes
    # The elements on the block are separated by underscores
    #
    # For example:
    # 72blsfihf-103585279100-Tumor_Tissue_RNAlater-WES_v1_TruSeqUDI_X-200320_A00559_AHJJVJDRXX-RHGM_LABKA_Fase-Fase01948_R1.fastq.gz

    # Creating the patient block
    patient = query.get_patient_by_id(db_conn, sample['patient_id']) if sample['patient_id'] else {'name': 'noinfo'}

    elements = [patient['name']]

    # Creating the sample block
    elements.append(__format_sample_number(sample['sample_number']))

    # Creating the tissue block
    sample_material = query.get_sample_material_by_name(db_conn, sample['type'])
    elements.append('_'.join([sample_material['type'],
                              sample_material['origin'],
                              sample_material['preservation']]))

    # Creating the library block
    elements.append(__create_library_assay_string(db_conn, sample, assay_shortcut))

    # Adding the seq info block place holder
    elements.append(const.SEQINFOPLACEHOLDER)

    # Creating the project block
    internal_info = 'RHGM' if project_info['internal'] else 'EXT'
    elements.append('_'.join([internal_info, 'LABKA', project_info['name']]))

    # Creating the custom block
    elements.append(__create_custom_block(project_info, sample))

    # Returning the concatentation of all blocks.
    return '-'.join(elements)


def __format_sample_number(sample_number: str) -> str:
    '''
    Returns the sample number formated on the right way.

    Arguments
    ---------
    sample_number: str
        The sample number to be formatted.
    number_of_occurrences: int
        The number of occurrences of the sample (in the database).

    Return
    ------
    str
        The formatted sample number.
    '''
    number = sample_number
    if match(r'\d+', number):
        number = '0' * (const.MIN_SAMPLE_NUMBER_LENGTH - len(number)) + number
    return number


def __format_patient_number(patient_number: str):
    '''
    Returns the patient number formated on the right way.

    Argument
    --------
    patient_number: str
        The patient number to be formatted.

    Return
    ------
    str
        The formatted patient number.
    '''
    number = patient_number
    if match(r'\d+', number):
        return '0' * (const.MIN_SAMPLE_NUMBER_LENGTH - len(number)) + number
    return number


def __create_library_assay_string(db_conn,
                                  sample_info: Mapping[str, object],
                                  assay_shortcut: str) -> str:
    '''
    Creates the library assay string.

    Arguments
    ---------
    db_conn
        The database connection object.
    sample_info: Mapping[str, object],
        A dictionary containing the sample attributes.
    assay_shortcut: str
        The assay short name.

    Return
    ------
    str
        A string composed by the following elements (separated by underscore):
        - the sample library name
        - the sample library version
        - the assay short name
        - X (a place holder for future usage)
    '''
    library = query.get_active_library_by_name(db_conn, sample_info['library'])
    library_number = query.get_sample_library_number(db_conn, sample_info['id'])
    library_id = __format_library_id(library['name'], library_number)
    elements = [library['name'], library['version']]
    elements.append(assay_shortcut)
    elements.append(library_id)
    return '_'.join(elements)

def __format_library_id(library: str, library_number: int) -> str:
    '''
    Format the library number.

    Arguments
    ---------
    library: str
        The library name.
    library_number: str
        The library number.
    '''
    if library_number:
        if library == 'WGS':
            return f'RHGM{library_number:05d}'
        if library == 'IDSNP':
            return f'RHGMSNP{library_number:05d}'
        raise RuntimeError('This case is not implemented!')
    return 'X'

def __create_custom_block(project_info, sample):

    formatted_patient_number = __format_patient_number(sample['patient_number'])

    # If the sample has external ids, the custom block is a composition
    # of internal and external ids
    if project_info['has_external_ids']:

        if project_info['name'] in const.STAGING_PROJECTS or \
           project_info['name'] in const.AKUT_PROJECTS:
            # Internal id
            elements = [project_info['name'] + formatted_patient_number]
            # External id
            elements.append(sample['external_ids'])
            return '_'.join(elements)

        if project_info['name'] in const.NGC_PROJECTS:
            # Project name + Subject ID (second identifier of the list)
            elements = [project_info['name'], sample['external_ids'].split(';')[1]]
            return '_'.join(elements)

        raise RuntimeError('If you arrived here, you must implement something that is missing')

    # If the project does not have external ids, we have to create a custom block
    # with the information that we have
    custom_block = []

    # If we have a pool information in the sample description we add it into the custom field
    pool_match = match(r'([A-Z]{3,5}(?:FFPE)?)\s*(\d{2,4})(?:FFPE)?', sample['description'])
    if pool_match:
        pool_type = pool_match.group(1)
        pool_number = pool_match.group(2)
        custom_block.append(f'{pool_type}{pool_number}')

    # If we have a non-empty patient number, we add it
    if formatted_patient_number:
        custom_block.append(project_info['name'] + formatted_patient_number)

    if not custom_block:
        custom_block.append('noinfo')
    
    return '_'.join(custom_block)


def __backup_file(file_path: Path) -> None:
    '''
    Make a backup of the given file.
    The file is remaded to <filename>.<timestamp>

    Argument
    --------
    file_path: Path
        The path for the file that should be renamed.
    '''
    if file_path.exists() and file_path.is_file():
        timestamp = datetime.now().strftime('%Y%m%d-%H%M%S')
        newfile = Path(file_path.absolute().as_posix() + '-' + timestamp)
        os.rename(file_path.as_posix(), newfile.as_posix())


def __write_file(file_path: Path, rows: List[str], eol: str = '\r\n') -> None:
    '''
    Auxiliary function for writing a list of rows into a file.
    If the destination file already exists, a backup file is created.

    Arguments
    ---------
    file_path: Path
        The path for the file.
    rows: List[str]
        The list of rows to be written into the file.
    eol: str
        The end of line delimiter that will be used to join the lines.
    '''
    # Making a backup if it already exists.
    __backup_file(file_path)
    # Writing the file content.
    with open(file_path, 'wb') as file_handler:
        file_handler.write(__generate_content_string(rows, eol))
