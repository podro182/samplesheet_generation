DROP TABLE IF EXISTS library;

CREATE TABLE IF NOT EXISTS library (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    name TEXT NOT NULL,
    version TEXT NOT NULL,
    active INTEGER DEFAULT 0
);

INSERT INTO library (name, version, active) VALUES
('Amplicon', 'v0', 1),
('BCP', 'v0', 1),
('BCM', 'v1', 1),
('BCSOM', 'v0', 1),
('CCL', 'v0', 0),
('CCP', 'v0', 1),
('CHOP', 'v0', 1),
('FANP', 'v0', 1),
('GMPANEL', 'v1', 1),
('HREP', 'v0', 1),
('IDSNPv1', 'v1', 0),
('IDSNP', 'v3', 1),
('JERN', 'v1', 1),
('MEK', 'v0', 1),
('RNA', 'v1', 1),
('SNPCUST003B1', 'v0', 0),
('TSO500', 'v1', 1),
('WES', 'v1', 1),
('WGS', 'v1', 1),
('other', 'v0', 1),
('scRNA', 'v1', 1);
