DROP TABLE IF EXISTS sample_material;

CREATE TABLE IF NOT EXISTS sample_material (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    name TEXT UNIQUE NOT NULL,
    ngc_name TEXT DEFAULT NULL,
    type TEXT NOT NULL,
    origin TEXT NOT NULL,
    preservation TEXT NOT NULL,
    active INT DEFAULT 0
);

INSERT INTO sample_material (name, ngc_name, type, origin, preservation, active) VALUES
('Blood', 'Blood', 'Normal', 'Blood', 'noinfo', 1),
('BloodEDTA', 'Blood', 'Normal', 'Blood', 'EDTA', 1),
('BloodCITRATE', 'Blood', 'Normal', 'Blood', 'CITRATE', 1),
('BloodSTRECK', 'Blood', 'Normal', 'Blood', 'STRECK', 1),
('BloodPAX', 'Blood', 'Normal', 'Blood', 'PAX', 1),
('Tumor', NULL, 'Tumor', 'Tissue', 'RNAlater', 1),
('Normal', NULL, 'Normal', 'Tissue', 'RNAlater', 1),
('TumorFFPE', NULL, 'Tumor', 'Tissue', 'FFPE', 1),
('NormalFFPE', NULL, 'Normal', 'Tissue', 'FFPE', 1),
('cfDNA', NULL, 'Tumor', 'Blood', 'cfDNA', 1),
('Prenatal', NULL, 'Normal', 'Prenatal', 'DNA', 1),
('Marv', NULL, 'Tumor', 'Marrow', 'EDTA', 1),
('FNA', NULL, 'Tumor', 'Tissue', 'FNA', 1),
('BAK', NULL, 'noinfo', 'Bacteria', 'DNA', 1),
('HPV', NULL, 'Tumor', 'Tissue', 'HPVPCR', 1),
('TumorBlood', NULL, 'Tumor', 'Blood', 'EDTA', 1),
('Mouthswab', NULL, 'Normal', 'Mouthswab', 'noinfo', 1);
