DROP TABLE IF EXISTS assay;
DROP TABLE IF EXISTS index1;
DROP TABLE IF EXISTS index2;
DROP TABLE IF EXISTS indexDouble;

CREATE TABLE IF NOT EXISTS assay (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    name TEXT UNIQUE NOT NULL,
    name_shortcut TEXT UNIQUE NOT NULL,
    active INTEGER DEFAULT 0,
    ngc_name TEXT DEFAUL NULL,
    adapter TEXT,
    adapter1 TEXT,
    adapter2 TEXT,
    singleIndex INTEGER,
    doubleIndex INTEGER,
    uniqueDI INTEGER
);

CREATE TABLE IF NOT EXISTS index1 (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    assay_id INTEGER NOT NULL,
    name TEXT NOT NULL,
    bases TEXT NOT NULL,
    FOREIGN KEY (assay_id) REFERENCES assay (id)
);

CREATE TABLE IF NOT EXISTS index2 (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    assay_id INTEGER NOT NULL,
    name TEXT NOT NULL,
    novabases TEXT NOT NULL,
    nextbases TEXT NOT NULL,
    FOREIGN KEY (assay_id) REFERENCES assay (id)
);

CREATE TABLE IF NOT EXISTS indexDouble (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    assay_id INTEGER NOT NULL,
    name TEXT NOT NULL,
    i7bases TEXT NOT NULL,
    i5novabases TEXT NOT NULL,
    i5nextbases TEXT NOT NULL,
    FOREIGN KEY (assay_id) REFERENCES assay (id)
);