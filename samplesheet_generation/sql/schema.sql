DROP TABLE IF EXISTS user;
DROP TABLE IF EXISTS samplesheet;
DROP TABLE IF EXISTS sample;
DROP TABLE IF EXISTS project;
DROP TABLE IF EXISTS assay;
DROP TABLE IF EXISTS reagentkit;
DROP TABLE IF EXISTS index1;
DROP TABLE IF EXISTS index2;
DROP TABLE IF EXISTS indexDouble;
DROP TABLE IF EXISTS constants;
DROP TABLE IF EXISTS word;
DROP TABLE IF EXISTS patient;
DROP TABLE IF EXISTS ngc_library;
DROP TABLE IF EXISTS library;
DROP TABLE IF EXISTS seqmachine;
DROP TABLE IF EXISTS sample_material;

CREATE TABLE IF NOT EXISTS user (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    username TEXT UNIQUE NOT NULL,
    password TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS samplesheet (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    fullname TEXT UNIQUE NOT NULL,
    author_id INTEGER NOT NULL,
    sequencer TEXT NOT NULL,
    run TEXT NOT NULL,
    flowcell TEXT,
    iemfileversion TEXT NOT NULL,
    date TEXT NOT NULL,
    workflow TEXT NOT NULL,
    application TEXT NOT NULL,
    assay TEXT NOT NULL,
    reagentkit TEXT NOT NULL,
    description TEXT,
    chemistry TEXT NOT NULL,
    reads1 TEXT NOT NULL,
    reads2 TEXT NOT NULL,
    umi TEXT,
    adapter1 TEXT NOT NULL,
    adapter2 TEXT NOT NULL,
    FOREIGN KEY (author_id) REFERENCES user (id)
);

CREATE TABLE IF NOT EXISTS sample (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    sample_uid TEXT NOT NULL,
    lane INTEGER,
    project_id INTEGER NOT NULL,
    patient_number TEXT,
    sample_number TEXT NOT NULL,
    patient_id INTEGER,
    type TEXT NOT NULL,
    library TEXT NOT NULL,
    index1 TEXT NOT NULL,
    index2 TEXT,
    description TEXT,
    samplesheet_id INTEGER NOT NULL,
    external_ids TEXT,
    FOREIGN KEY (project_id) REFERENCES project (id),
    FOREIGN KEY (samplesheet_id) REFERENCES samplesheet (id),
    FOREIGN KEY (patient_id) REFERENCES patient (id)
);

CREATE TABLE IF NOT EXISTS project (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    author_id INTEGER NOT NULL,
    name TEXT UNIQUE NOT NULL,
    internal INTEGER,
    has_glass_no INTEGER,
    has_external_ids INTEGER DEFAULT 0,
    contact TEXT NOT NULL,
    contact_mail TEXT NOT NULL,
    description TEXT NOT NULL,
    FOREIGN KEY (author_id) REFERENCES user (id)
);

CREATE TABLE IF NOT EXISTS assay (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    name TEXT UNIQUE NOT NULL,
    name_shortcut TEXT UNIQUE NOT NULL,
    active INTEGER DEFAULT 0,
    ngc_name TEXT DEFAULT NULL,
    adapter TEXT,
    adapter1 TEXT,
    adapter2 TEXT,
    singleIndex INTEGER,
    doubleIndex INTEGER,
    uniqueDI INTEGER
);

CREATE TABLE IF NOT EXISTS reagentkit (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    seqmodel TEXT NOT NULL,
    version TEXT NOT NULL,
    novaseqbases INTEGER NOT NULL,
    is_default INTEGER NOT NULL,
    active INTEGER NOT NULL
);

CREATE TABLE IF NOT EXISTS index1 (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    assay_id INTEGER NOT NULL,
    name TEXT NOT NULL,
    bases TEXT NOT NULL,
    FOREIGN KEY (assay_id) REFERENCES assay (id)
);

CREATE TABLE IF NOT EXISTS index2 (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    assay_id INTEGER NOT NULL,
    name TEXT NOT NULL,
    novabases TEXT NOT NULL,
    nextbases TEXT NOT NULL,
    FOREIGN KEY (assay_id) REFERENCES assay (id)
);

CREATE TABLE IF NOT EXISTS indexDouble (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    assay_id INTEGER NOT NULL,
    name TEXT NOT NULL,
    i7bases TEXT NOT NULL,
    i5novabases TEXT NOT NULL,
    i5nextbases TEXT NOT NULL,
    FOREIGN KEY (assay_id) REFERENCES assay (id)
);

CREATE TABLE IF NOT EXISTS word (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    name TEXT UNIQUE NOT NULL,
    used INTEGER NOT NULL
);

CREATE TABLE IF NOT EXISTS patient (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    word_id INTEGER UNIQUE NOT NULL,
    cpr_no INTEGER UNIQUE NOT NULL,
    name TEXT UNIQUE NOT NULL,
    FOREIGN KEY (word_id) REFERENCES words (id)
);

CREATE TABLE IF NOT EXISTS ngc_library (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    library_type TEXT NOT NULL,
    library_number INTEGER NOT NULL,
    sample_id INTEGER UNIQUE NOT NULL,
    FOREIGN KEY (sample_id) REFERENCES sample (id)
);

CREATE TABLE IF NOT EXISTS library (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    name TEXT NOT NULL,
    version TEXT NOT NULL,
    active INTEGER DEFAULT 0
);

CREATE TABLE IF NOT EXISTS seqmachine (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    code TEXT UNIQUE NOT NULL,
    name TEXT UNIQUE NOT NULL,
    model TEXT NOT NULL,
    active INTEGER DEFAULT 0
);

CREATE TABLE IF NOT EXISTS sample_material (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    name TEXT UNIQUE NOT NULL,
    ngc_name TEXT DEFAULT NULL,
    type TEXT NOT NULL,
    origin TEXT NOT NULL,
    preservation TEXT NOT NULL,
    active INT DEFAULT 0
);
