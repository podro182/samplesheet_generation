DROP TABLE IF EXISTS reagentkit;

CREATE TABLE IF NOT EXISTS reagentkit (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    seqmodel TEXT NOT NULL,
    version TEXT NOT NULL,
    novaseqbases INTEGER NOT NULL,
    is_default INTEGER NOT NULL,
    active INTEGER NOT NULL
);

INSERT INTO reagentkit (seqmodel, version, novaseqbases, is_default, active) VALUES
('HiSeq', '1.0', 1, 1, 1),
('MiSeq', '1.0', 1, 1, 1),
('NextSeq', '1.0', 0, 1, 1),
('NovaSeq', '1.0', 1, 0, 1),
('NovaSeq', '1.5', 0, 0, 1);
