DROP TABLE IF EXISTS seqmachine;

CREATE TABLE IF NOT EXISTS seqmachine (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    code TEXT NOT NULL,
    name TEXT NOT NULL,
    active INTEGER DEFAULT 0,
    model TEXT NOT NULL
);

INSERT INTO seqmachine (code, name, active, model) VALUES
('D00140', 'HiSeq D00140', 1, 'HiSeq'),
('M01115', 'MiSeq M01115 - Zlatan', 1, 'MiSeq'),
('M03392', 'MiSeq M03392 - Messi',  1, 'MiSeq'),
('M04671', 'MiSeq M04671 - Ronaldo',  1, 'MiSeq'),
('NB501792', 'NextSeq NB501792 - Beyonce',  1, 'NextSeq'),
('NS500314', 'NextSeq NS500314 - Victoria',  1, 'NextSeq'),
('A00281', 'NovaSeq A00281 - RH',  1, 'NovaSeq'),
('A00559', 'NovaSeq A00559 - KC',  1, 'NovaSeq'),
('A01176', 'NovaSeq A01176 - KC',  1, 'NovaSeq');
