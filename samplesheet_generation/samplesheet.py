# -*- coding: utf-8 -*-

#########################################################################################
# Copyright (C) 2020 GENOMIC MEDICINE - RIGSHOSPITALET                                  #
#########################################################################################

'''
Module that provide the "backend" methods for the sample sheet creation/visualisation
pages.

Copyright (C) 2020 GENOMIC MEDICINE - RIGSHOSPITALET

Authors : Bioinformatics team of the Genomic Medicine Department @ RIGSHOSPITALET
- Savvas Kinalis <savvas.kinalis@regionh.dk>
- Mathias Husted Torp <mathias.husted.torp@regionh.dk>
- Filipe Garrett Vieira <filipe.garrett.vieira@regionh.dk>
- Christian Baudet <christian.baudet.01@regionh.dk>
'''

#########################################################################################
# Import section                                                                        #
#########################################################################################
from re import match

from flask import (Blueprint, g, current_app, redirect, render_template,
                   request, url_for, session)

from samplesheet_generation import csv
from samplesheet_generation import constants as const
from samplesheet_generation.auth import login_required
from samplesheet_generation.db import get_db, query, insert_update

# from samplesheet_generation.forms.samplesheet import get_sample_creation_form_class
from samplesheet_generation.labka_lookup import get_labka_info_from_glassno
from samplesheet_generation.forms.samplesheet import get_all_sample_creation_form
from samplesheet_generation.forms.samplesheet import get_samplesheet_view_form_class
from samplesheet_generation.forms.samplesheet import get_samplesheet_creation_form_class

#########################################################################################
# Definition of the blueprint object.
blueprint = Blueprint('samplesheet', __name__, url_prefix='/samplesheet') #pylint:disable=invalid-name
#########################################################################################

#########################################################################################
# Public routing functions section                                                      #
#########################################################################################

@blueprint.route('/')
@login_required
def index():
    '''
    Renders the page with the list of sample sheets that are registered into the system.
    '''
    db_conn = get_db()
    samplesheets = query.get_fullname_of_all_samplesheets(db_conn)
    return render_template('samplesheet/index.html', samplesheets=samplesheets)


@blueprint.route('/<fullname>/view', methods=('GET', 'POST'))
@login_required
def view(fullname):
    '''
    Renders the visualisation page for the sample sheet that has the given full name.
    '''

    db_conn = get_db()

    samplesheet = query.get_samplesheet_by_fullname(db_conn, fullname)
    samples = query.get_samples_by_samplesheet_id(db_conn, samplesheet['id'])

    samples = [
        {'project_name': query.get_project_by_id(db_conn, sample['project_id'])['name'],
         'lane': sample['lane'],
         'sample_number': sample['sample_number'],
         'patient_number': sample['patient_number'],
         'sample_type': sample['type'],
         'library': sample['library'],
         'index1': sample['index1'],
         'index2': sample['index2'],
         'description': sample['description']}
        for sample in samples]

    for sample in samples:
        for key in sample:
            if sample[key] is None:
                sample[key] = ''

    form_class = get_samplesheet_view_form_class()
    samplesheet_form = form_class(request.form)
    if request.method == 'GET':
        splits = fullname.split('_')
        sequencer = splits[1]
        run = splits[2]
        samplesheet_form.default(sequencer, run, samplesheet['assay'])

    if request.method == 'POST':
        insert_update.delete_samplesheet(db_conn, fullname)
        db_conn.commit()
        return redirect(url_for('samplesheet.index'))

    return render_template('samplesheet/view.html',
                           samplesheet=samplesheet_form,
                           samples=samples)


@blueprint.route('/create/1', methods=('GET', 'POST'))
@login_required
def create1():
    '''
    Processes GET and POST requests from the sample sheet creation form (1st page).
    '''
    # Creating the form
    form_class = get_samplesheet_creation_form_class()
    form = form_class(formdata=request.form)

    if request.method == 'GET':
        # If it is a GET request, set the date to the current one
        form.default_date()

    if request.method == 'POST' and form.validate():
        # POST request and the form validation was ok.

        # Adding the form data to the session
        session['create1'] = form.data
        session['create1']['date'] = session['create1']['date'].strftime('%y%m%d')

        # Redirecting to the second form (samples creation form)
        return redirect(url_for('samplesheet.create2'))

    # Rendering the page with the current form data (new empty form or filled form with errors)
    return render_template('samplesheet/create/1.html', form=form)


@blueprint.route('/create/2', methods=('GET', 'POST'))
@login_required
def create2():
    '''
    Processes GET and POST requests from the sample sheet creation form (2nd page).
    '''

    if not session.get('create1'):
        # The session does not contain data from the 1st page.
        # Redirecting to the fist form page.
        return redirect(url_for('samplesheet.create1'))

    # Retrieving from the session the data that was submitted through the 1st form.
    form_samplesheet = session['create1']

    # Creating the main form that is composed by N SampleCreationForms
    form_class = get_all_sample_creation_form(int(form_samplesheet['samples_num']))

    # Parsing the request to populate the form object
    form = form_class(request.form)

    # Getting the database connection handler.
    db_conn = get_db()

    if request.method == 'POST' and form.validate(session['create1']['assay']):
        # We have a POST request whose validation is OK


        # Registering the sample sheet and its sample into the database
        samplesheet_id = __register_samplesheet(db_conn, form_samplesheet, form)

        # Creating the sample sheet file
        csv.create_samplesheet_file(db_conn, samplesheet_id,
                                    current_app.config)

        # Commit only after everything went fine
        db_conn.commit()

        # Removing the 1st form data from the session
        del session['create1']

        # Redirecting the application to the sample sheet list page
        return redirect(url_for('samplesheet.index'))

    # Rendering the page with the current form data (new empty form or filled form with errors)
    return render_template('samplesheet/create/2.html',
                           form=form,
                           lib_version_dict=__create_library_version_dictionary(db_conn),
                           tissue_dict=__create_tissue_dictionary(db_conn))


#########################################################################################
# Private functions section                                                             #
#########################################################################################

def __register_samplesheet(db_conn, form_samplesheet, form_samplelist) -> int:

    # Recovering the assay information
    assay_info = query.get_assay_by_name(db_conn, form_samplesheet['assay'])
    assay_shortcut = assay_info['name_shortcut']

    # Creating the full name of the sample sheet
    fullname = __create_samplesheet_fullname(form_samplesheet, assay_shortcut)

    # Correcting the adapter information (if necessary)
    # Basically, replaces 'Default' by the right DNA sequence
    __correct_adapters(db_conn, form_samplesheet)

    # Inserting the sample sheet into the database
    samplesheet_info = insert_update.insert_samplesheet(db_conn, fullname, g.user['id'],
                                                        form_samplesheet['sequencer'],
                                                        form_samplesheet['run'],
                                                        form_samplesheet['flowcell'],
                                                        const.IEMFILEVERSION,
                                                        form_samplesheet['date'],
                                                        const.WORKFLOW,
                                                        const.APPLICATION,
                                                        form_samplesheet['assay'],
                                                        form_samplesheet['reagentkit'],
                                                        form_samplesheet['description'],
                                                        form_samplesheet['chemistry'],
                                                        form_samplesheet['reads'],
                                                        form_samplesheet['reads'],
                                                        form_samplesheet['umi'],
                                                        form_samplesheet['adapter1'],
                                                        form_samplesheet['adapter2'])

    # Inserting each one of the samples into the database
    samples = form_samplelist.data.get('samples')
    nsamples = len(samples)
    position = 0

    # List of NGC Samples for post-processing
    while position < nsamples:

        sample = samples[position]
        position = position + 1

        project_info = query.get_project_by_name(db_conn,
                                                 sample['project'])

        (patient_id, external_ids) = __patient_and_external_identifiers(db_conn,
                                                                        sample,
                                                                        project_info)

        persisted = insert_update.insert_sample(db_conn, samplesheet_info,
                                                position,
                                                sample['patient_number'],
                                                sample['lane'],
                                                sample['sample_number'],
                                                sample['sample_type'],
                                                sample['library'],
                                                sample['project'],
                                                sample['index1'],
                                                sample['index2'],
                                                sample['description'],
                                                patient_id,
                                                external_ids)

        if project_info['name'] in const.NGC_PROJECTS:
            __create_ngc_library_id(db_conn, persisted)

    return samplesheet_info['id']


def __create_samplesheet_fullname(form_samplesheet, assay_shortcut: str) -> str:
    '''
    Creates the sample sheet full name using the given data.

    Arguments
    ---------
    form_samplesheet
        The sample sheet creation form object (1st form data)
    assay_shortcut: str
        The assay short name.

    Return
    ------
    str
        The full name of the sample sheet.
    '''
    elements = [form_samplesheet['date'], form_samplesheet['sequencer'],
                form_samplesheet['run'], form_samplesheet['flowcell'],
                assay_shortcut]
    return '_'.join(elements)


def __correct_adapters(db_conn, form_samplesheet) -> None:
    '''
    Replaces the 'Default' string by the right adapter sequences.

    Arguments
    ---------
    db_conn
        The database connection handler.
    form_samplesheet
        The sample sheet creation form object (1st form data)
    '''
    if form_samplesheet['adapter1'] == 'Default' and form_samplesheet['adapter2'] == 'Default':
        ad1, ad2 = query.get_assay_adapters(db_conn, form_samplesheet['assay'])
        form_samplesheet['adapter1'] = ad1
        form_samplesheet['adapter2'] = ad2


def __patient_and_external_identifiers(db_conn, sample: dict, project_info: dict) -> (int, str):
    '''
    Recover/Create patient identifier when need based on the patient CPR.
    Recover from labka external identifiers that are related with the sample.

    Arguments
    ---------
    db_conn
        The database connection handler.
    sample: dict
        The sample information.
    project_info: dict

    Return
    ------
    (int, str)
        A 2-tuple containing:
        1 - (int) the primary key that identifies the patient;
        2 - (str) the string with eventual externals identifiers for the sample;
    '''
    patient_id = None
    external_ids = None
    if project_info['has_glass_no']:
        # Recover the labka information
        labka_info = get_labka_info_from_glassno(sample['sample_number'])

        # Create/Retrieve GM's patient id
        patient_id = __get_or_generate_patient_id_from_cpr(db_conn,
                                                           labka_info[const.LABKA_CPR])

        # Retrieve the external identifier that is stored in labka
        external_ids = __retrieve_external_ids(project_info, labka_info)

    return (patient_id, external_ids)


def __get_or_generate_patient_id_from_cpr(db_conn, cpr_no: int) -> int:
    """
    Generates a patient object from the sample number, aka glass number
    and enters the information to the database

    :param db_conn:
        the database connection handler

    :param cpr_no:
        CPR number in integer form (no dash in the middle).
    :return:
        Str
    """
    # Check if patient_id is assigned to this CPR no.
    patient_info = query.get_patient_by_cpr(db_conn, cpr_no)
    if not patient_info:
        # Find pronounceable word to use from the word table and mark it as used.
        word_info = query.get_word_unused(db_conn)
        insert_update.update_word_as_used(db_conn, word_info['id'])
        # Generate the patient id and insert into the database.
        # Handle the case for people from Faroes islands
        if cpr_no[-1] == 'M':
            sex = 'm'
        elif cpr_no[-1] == 'K':
            sex = 'f'
        else:
            # General case
            sex = 'f' if (int(cpr_no[-1]) % 2 == 0) else 'm'
        patient_name = "{yy}{wwwwww}{s}".format(yy=cpr_no[4:6],
                                                wwwwww=word_info['name'],
                                                s=sex)
        patient_info = insert_update.insert_patient(db_conn,
                                                    word_info['id'],
                                                    cpr_no,
                                                    patient_name)
    return patient_info['id']


def __retrieve_external_ids(project_info: dict, labka_info: dict) -> str:
    '''
    Retrieve from labka the external ids of the sample.

    Arguments
    ---------
    project_info: dict
        The sample project information.
    labka_info: dict
        The information that was retrieved from labka for the sample.

    Return
    ------
    str
        A string with the external identifiers.
    '''
    if not project_info['has_external_ids']:
        return None

    alternative = labka_info[const.LABKA_SAMPLE_ALTERNATIVE_NAME]
    if not alternative:
        raise RuntimeError('Unexpected error! Something changed in the middle of the process.')

    alternative = alternative.strip()
    if project_info['name'] in const.STAGING_PROJECTS:
        matcher = match(const.STAGING_EXTERNAL_ID_REGEX, alternative)
        if not matcher:
            raise RuntimeError('Unexpected error! Something changed in the middle of the process.')
        # x enter in the place of underscores
        return alternative.replace('_', 'x')

    if project_info['name'] in const.AKUT_PROJECTS:
        matcher = match(const.AKUT_EXTERNAL_ID_REGEX, alternative)
        if not matcher:
            raise RuntimeError('Unexpected error! Something changed in the middle of the process.')
        elements = [matcher.group(1), matcher.group(2)]
        if matcher.group(3):
            elements.append(matcher.group(3))
        # x enter in the place of dashes and spaces
        return 'x'.join(elements)

    if project_info['name'] in const.NGC_PROJECTS:
        alternative = alternative.replace(';', ',')
        ids = alternative.split(',')
        if len(ids) < 2:
            raise RuntimeError('Unexpected error! Something changed in the middle of the process.')
        matcher1 = match(const.NGC_REQUISITION_ID_REGEX, ids[0].strip())
        matcher2 = match(const.NGC_SUBJECT_ID_REGEX, ids[1].strip())
        if not matcher1 or not matcher2:
            raise RuntimeError('Unexpected error! Something changed in the middle of the process.')
        return ';'.join([ids[0].strip(), ids[1].strip()])

    raise RuntimeError('If you arrived here, you must implement something that is missing.')


def __create_ngc_library_id(db_conn, persisted_sample):
    '''
    Create a NGC library ID entry for the given sample.

    Arguments
    ---------
    db_conn
        The database connection handler.
    persisted_sample
        The dictionary that represents the persisted sample.
    '''
    description = persisted_sample['description']
    if not description or 'newlibrary' in description.lower():
        # In this case, we assume that we will have a new library
        library_number = query.get_next_library_number(db_conn, persisted_sample['library'])
    else:
        # In this case, we are going to use a old library number
        matcher = match(const.FLOWCELL_REGEX, description)
        if not matcher:
            matcher = match(const.MISEQ_FLOWCELL_REGEX, description)
            if not matcher:
                raise RuntimeError('Unexpected error! Something changed in the middle of the process.')#pylint:disable=line-too-long
        library_number = query.get_library_number(db_conn,
                                                  matcher.group(1),
                                                  persisted_sample['library'],
                                                  persisted_sample['sample_number'],
                                                  persisted_sample['index1'],
                                                  persisted_sample['index2'])
        if not library_number:
            raise RuntimeError('Unexpected error! Something changed in the middle of the process.')

    insert_update.insert_ngc_library(db_conn,
                                     persisted_sample['library'],
                                     library_number,
                                     persisted_sample['id'])


def __create_library_version_dictionary(db_conn) -> dict:
    '''
    This function creates the library version dictionary for the javascript
    script that is located on the create samples page.

    Arguments
    ---------
    db_conn
        The database connection handler.

    Return
    ------
    dict
        A dictionary where the key is the library name and the value is the
        library version.
    '''
    to_return = {}
    libraries = query.get_libraries(db_conn, True)
    for library in libraries:
        to_return[library['name']] = library['version']
    return to_return


def __create_tissue_dictionary(db_conn) -> dict:
    '''
    This function creates the tissue dictionary for the javascript
    script that is located on the create samples page.

    Arguments
    ---------
    db_conn
        The database connection handler.

    Return
    ------
    dict
        A dictionary where the key is the short tissue type name and
        the value is a composition of tissue type, tissue origin and
        tissue preservation.
    '''
    to_return = {}
    sample_materials = query.get_sample_materials(db_conn, True)
    for material in sample_materials:
        to_return[material['name']] = ' '.join([material['type'],
                                                material['origin'],
                                                material['preservation']])
    return to_return
