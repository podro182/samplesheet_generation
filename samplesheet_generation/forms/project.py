# -*- coding: utf-8 -*-

#########################################################################################
# Copyright (C) 2020 GENOMIC MEDICINE - RIGSHOSPITALET                                  #
#########################################################################################

'''
Module that contains the functions/classes related with the create sample sheet forms.

Copyright (C) 2020 GENOMIC MEDICINE - RIGSHOSPITALET

Authors : Bioinformatics team of the Genomic Medicine Department @ RIGSHOSPITALET
- Savvas Kinalis <savvas.kinalis@regionh.dk>
- Mathias Husted Torp <mathias.husted.torp@regionh.dk>
- Filipe Garrett Vieira <filipe.garrett.vieira@regionh.dk>
- Christian Baudet <christian.baudet.01@regionh.dk>
'''

#########################################################################################
# Import section                                                                        #
#########################################################################################

from wtforms import validators
from wtforms import Form, BooleanField, StringField, TextAreaField

from samplesheet_generation.db import get_db, query

from samplesheet_generation.forms.validators import ValidateLength
from samplesheet_generation.forms.validators import ValidateProjectName

#pylint:disable=too-few-public-methods

#########################################################################################
# Classes section                                                                       #
#########################################################################################

class ProjectCreationForm(Form):
    '''
    This class models the project creation form.
    '''
    name = StringField('Project name',
                       [ValidateLength(min_length=3, max_length=25),
                        validators.Regexp('^[a-zA-Z0-9]+$',
                                          message='Only alphabetic characters.'),
                        ValidateProjectName(should_exist=False)])

    internal = BooleanField('Internal project', default="checked")

    has_glass_no = BooleanField('Samples have glass number', default='checked')

    contact = StringField('Contact person',
                          [ValidateLength(min_length=4, max_length=50)])

    contact_mail = StringField('Contact person e-mail',
                               [validators.Email(message='This is not a valid e-mail address.')])

    description = TextAreaField('Description',
                                [ValidateLength(min_length=50, max_length=2000)])


class ProjectEditForm(Form):
    '''
    This class models the project edit form.
    '''

    name = StringField('Project name',
                       [ValidateLength(min_length=3, max_length=25)])

    internal = BooleanField('Internal project', default="checked")

    has_glass_no = BooleanField('Samples have glass number', default='checked')

    contact = StringField('Contact person',
                          [ValidateLength(min_length=4, max_length=50)])

    contact_mail = StringField('Contact person e-mail',
                               [validators.Email(message='This is not a valid e-mail address.')])

    description = TextAreaField('Description',
                                [ValidateLength(min_length=50, max_length=2000)])

    def default(self, project_name):
        '''
        Configures the form with the initial default values.
        '''
        project = query.get_project_by_name(get_db(), project_name)
        self.name.data = project['name']
        self.internal.data = project['internal']
        self.has_glass_no.data = project['has_glass_no']
        self.contact.data = project['contact']
        self.contact_mail.data = project['contact_mail']
        self.description.data = project['description']
