# -*- coding: utf-8 -*-

#########################################################################################
# Copyright (C) 2020 GENOMIC MEDICINE - RIGSHOSPITALET                                  #
#########################################################################################

'''
Module that contains the functions/classes related with the create sample sheet forms.

Copyright (C) 2020 GENOMIC MEDICINE - RIGSHOSPITALET

Authors : Bioinformatics team of the Genomic Medicine Department @ RIGSHOSPITALET
- Savvas Kinalis <savvas.kinalis@regionh.dk>
- Mathias Husted Torp <mathias.husted.torp@regionh.dk>
- Filipe Garrett Vieira <filipe.garrett.vieira@regionh.dk>
- Christian Baudet <christian.baudet.01@regionh.dk>
'''

#########################################################################################
# Import section                                                                        #
#########################################################################################
from re import match
from typing import List, Set
from datetime import date
from collections import Counter

from wtforms import Form, validators
from wtforms import StringField, SelectField, DateField, FieldList, FormField

from samplesheet_generation.db import get_db, query
from samplesheet_generation import constants as const
from samplesheet_generation.forms.validators import ValidateProjectName
from samplesheet_generation.labka_lookup import get_labka_info_from_glassno

#pylint:disable=too-few-public-methods,line-too-long

#########################################################################################
# Constants section                                                                     #
#########################################################################################

# Some default regex
EMPTY = r'^$'
LENIENT = r'^\w+$'
DEFAULT_GLASSNR = r'^(\d{1,5}|(\d\d){4,6}(\w{0,4})?)$'
DEFAULT_PATIENTNR = r'^\d*$'

# Mapping: project to glassnumber regex format
PROJECT_SAMPLE_NUMBER_EXCEPTIONS_FORMAT = {
    'DNABAKWGS' : r'^[A-Za-z0-9]+$',
    'JONLP' : r'^\d+[A-Z]?$',
    'NEOPEP' : r'^AA1833_\w+$',
    'PRO34' : r'^\d+[A-Z]?$',
    'PR081': LENIENT,
    'Staging' : r'\d+_\d+_[A-Z]',
    'Zeynep' : r'^\d+[A-Z]$'
}

# Mapping: project to patient number regex format
PROJECT_PATIENTNR_FORMAT = {
    'PRO38': DEFAULT_PATIENTNR,
    'PR063': LENIENT,
    'PR076': LENIENT,
    'PR075': LENIENT,
    'PR081': DEFAULT_PATIENTNR,
    'NEOPEP' : EMPTY,
    'NGC' : EMPTY,
    'PANEL' : r'\d{0,5}[A-Z]{0,3}$'
}

# Mapping: project to sample type set (or regex)
PROJECT_SAMPLE_TYPE = {
    'DNABAKWGS': {'BAK'},
    'HPV' : {'HPV'},
    'P021' : {'BAK'},
    'NGC' : {'Blood', 'BloodEDTA', 'BloodCITRATE', 'BloodSTRECK', 'BloodPAX'}
}

# Mapping: project to library
PROJECT_LIBRARY = {
    'NGC' : {'IDSNP', 'WGS'}
}

#pylint:disable=too-many-instance-attributes,too-many-arguments

def get_samplesheet_creation_form_class():
    '''
    This function creates the sample sheet creation form:
    1- It first defines the class and its methods.
    2- Then, it adds all attributes to the class. This is done like that because
       of the online retrieval of database information for the select fields.
    3- It returns the class
    '''

    class SamplesheetCreationForm(Form):
        '''
        Class that implements the Sample Sheet Creation form.
        (1st form on the sample sheet creation workflow).
        '''
        def default_date(self):
            '''
            Initialises the default date.
            '''
            self.date.data = date.today()


        def __validate_reagentkit_matches_seqmodel(self) -> bool:
            '''
            Validates that the reagent kit version matches the sequencing machine model.
            Also sets a value to the reagent kit if "Default" is chosen.
            '''
            seqmachine = query.get_sequencing_machine_by_code(get_db(), self.sequencer.data)

            if self.reagentkit.data == 'Default':

                # If the default reagent kit is chosen, set the default from the database
                self.reagentkit.data = query.get_reagentkit_default_by_seqmachine(
                    get_db(), seqmachine['model'])

                if not self.reagentkit.data:
                    self.reagentkit.errors.append('This sequencing machine model does not have a '\
                                                  'default reagent kit version. Please pick from the list.')
                    return False
            else:
                # If a specific reagent kit version is chosen, check if it matches the database entries
                novaseqbases = query.get_novaseqbases_by_model_reagentkit(
                    get_db(), seqmachine['model'], self.reagentkit.data)

                if novaseqbases is None:
                    self.reagentkit.errors.append('Reagent kit version does not fit with the sequencer.')
                    return False
            return True


        def validate(self):
            '''
            Validates the form data.
            '''
            # First - buil-in form validation
            if not Form.validate(self):
                return False

            # Custom form validation

            # Existing samplesheet
            sheet = query.get_samplesheet_by_sequencer_run_assay(get_db(),
                                                                 self.sequencer.data,
                                                                 self.run.data,
                                                                 self.assay.data)
            if sheet is not None:
                self.sequencer.errors.append('Samplesheet seems to already exist!')
                return False

            # Custom adapter pair validation (1 default and 2 not default)
            if self.adapter1.data == 'Default' and self.adapter2.data != 'Default':
                msg = "Since Adapter read 2 has a value, Adapter read 1 should not be 'Default'."
                self.adapter1.errors.append(msg)
                return False

            # Custom adapter pair validation (1 not default and 2 default)
            if self.adapter1.data != 'Default' and self.adapter2.data == 'Default':
                msg = """Since Adapter read 1 has a value, Adapter read 2 should not be 'Default'.
                         If there is only one adapter, leave Adapter read 2 empty."""
                self.adapter2.errors.append(msg)
                return False

            # Valid sequencing machine and reagent kit pair
            if not self.__validate_reagentkit_matches_seqmodel():
                return False

            # If we arrived here. Hurray!
            return True

    # End of class definition - Begin of attributes definition

    # Field - select - sequencing machine
    setattr(SamplesheetCreationForm,
            'sequencer',
            SelectField('Sequencer', choices=Choices.sequencing_machines(True)))

    # Field - input text - run number.
    setattr(SamplesheetCreationForm,
            'run',
            StringField('Run number',
                        [validators.Regexp('^[0-9]{4}$', message='Run number must be 4 digits long.')]))

    # Field - input text - flowcell number.
    setattr(SamplesheetCreationForm,
            'flowcell',
            StringField('Flow Cell',
                        [validators.Regexp('^[A-Z0-9]{5}([A-Z0-9]{4})?$', message='Flow Cell serial number must be 5 or 9 characters long.')]))

    # Field - input text - run date.
    setattr(SamplesheetCreationForm,
            'date',
            DateField('Date of run', format='%y%m%d'))

    # Field - select field - active assays
    setattr(SamplesheetCreationForm,
            'assay',
            SelectField('Assay', choices=Choices.assays(True)))

    # Field - select field - active reagent kit version
    setattr(SamplesheetCreationForm,
            'reagentkit',
            SelectField('Reagent kit', choices=Choices.reagentkit(True)))

    # Field - input text - Description
    setattr(SamplesheetCreationForm,
            'description',
            StringField('Description'))

    # Field - input text - Chemistry
    setattr(SamplesheetCreationForm,
            'chemistry',
            StringField('Chemistry', [validators.InputRequired()], default='Default'))

    # Field - input text - read length
    setattr(SamplesheetCreationForm,
            'reads',
            StringField('Reads', [validators.Regexp('^[1-9][0-9]+$', message='Digits only.')], default='151'))

    # Field - input text - length of UMI reads
    setattr(SamplesheetCreationForm,
            'umi',
            StringField('Length of read UMIs', [validators.Regexp('^[0-9]$', message='Digits only.')], default='0'))

    # Field - input text - adapter1 sequence
    setattr(SamplesheetCreationForm,
            'adapter1',
            StringField('Adapter read1',
                        [validators.Regexp('^(Default|[ATCG]+)$', message='Sequence of ATCG.')],
                        default='Default'))

    # Field - input text - adapter2 sequence
    setattr(SamplesheetCreationForm,
            'adapter2',
            StringField('Adapter read2',
                        [validators.Regexp('^(Default|[ATCG]+)$', message='Sequence of ATCG.')],
                        default='Default'))

    # Field - input text - number of samples
    setattr(SamplesheetCreationForm,
            'samples_num',
            StringField('Number of samples',
                        [validators.Regexp('^[1-9][0-9]*$', message='Digits only.')],
                        default=''))

    return SamplesheetCreationForm


def get_samplesheet_view_form_class():
    '''
    This function creates the sample sheet view form:
    1- It first defines the class and its methods.
    2- Then, it adds all attributes to the class. This is done like that because
       of the online retrieval of database information for the select fields.
    3- It returns the class
    '''
    class SamplesheetViewForm(Form):
        '''
        Class that implements the Sample Sheet Visualisation form.
        '''
        def default(self, sequencer: str, run: str, assay: str) -> None:
            '''
            Retrieves the sample sheet data and populates the form with the
            initial values.

            Arguments
            ---------
            sequencer: str
                The sequencing machine code.
            run:
                The sequencing run number.
            assay: str
                The assay name.
            '''
            sheet = query.get_samplesheet_by_sequencer_run_assay(get_db(), sequencer, run, assay)
            self.sequencer.data = sheet['sequencer']
            self.run.data = sheet['run']
            self.flowcell.data = sheet['flowcell']
            self.date.data = date(int('20'+sheet['date'][0:2]),
                                  int(sheet['date'][2:4]),
                                  int(sheet['date'][4:6]))
            self.assay.data = sheet['assay']
            self.reagentkit.data = sheet['reagentkit']
            self.description.data = sheet['description']
            self.chemistry.data = sheet['chemistry']
            self.reads.data = sheet['reads1']
            self.umi.data = sheet['umi']
            self.adapter1.data = sheet['adapter1']
            self.adapter2.data = sheet['adapter2']

    # End of class definition - Begin of attributes definition

    # Field - select - sequencing machine
    setattr(SamplesheetViewForm,
            'sequencer',
            SelectField('Sequencer', choices=Choices.sequencing_machines(False)))

    # Field - input text - run number.
    setattr(SamplesheetViewForm,
            'run',
            StringField('Run number',
                        [validators.Regexp('^[0-9]{4}$',
                        message='Run number must be 4 digits long.')]))

    # Field - input text - flowcell number.
    setattr(SamplesheetViewForm,
            'flowcell',
            StringField('Flow Cell',
                        [validators.Regexp('^[A-Z0-9]{5}([A-Z0-9]{4})?$',
                        message='Flow Cell serial number must be 5 or 9 characters long.')],
                        default=''))

    # Field - input text - run date.
    setattr(SamplesheetViewForm,
            'date',
            DateField('Date of run', format='%y%m%d'))

    # Field - select field - Assays
    setattr(SamplesheetViewForm,
            'assay',
            SelectField('Assay', choices=Choices.assays(False)))

    # Field - select field - active reagent kit version
    setattr(SamplesheetViewForm,
            'reagentkit',
            SelectField('Reagent kit', choices=Choices.reagentkit(False)))

    # Field - input text - Description
    setattr(SamplesheetViewForm,
            'description',
            StringField('Description'))

    # Field - input text - Chemistry
    setattr(SamplesheetViewForm,
            'chemistry',
            StringField('Chemistry', [validators.InputRequired()], default='Default'))

    # Field - input text - read length
    setattr(SamplesheetViewForm,
            'reads',
            StringField('Reads',
                        [validators.Regexp('^[1-9][0-9]+$', message='Digits only.')],
                        default='151'))

    # Field - input text - length of UMI reads
    setattr(SamplesheetViewForm,
            'umi',
            StringField('Length of read UMIs',
                        [validators.Regexp('^[0-9]$', message='Digits only.')],
                        default='0'))

    # Field - input text - adapter1 sequence
    setattr(SamplesheetViewForm,
            'adapter1',
            StringField('Adapter read1',
                        [validators.Regexp('^(Default|[ATCG]+)$', message='Sequence of ATCG.')],
                        default='AGATCGGAAGAGCACACGTCTGAACTCCAGTCA'))

    # Field - input text - adapter2 sequence
    setattr(SamplesheetViewForm,
            'adapter2',
            StringField('Adapter read2',
                        [validators.Regexp('^(Default|[ATCG]+)$', message='Sequence of ATCG.')],
                        default='AGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGT'))

    # Field - input text - number of samples
    setattr(SamplesheetViewForm,
            'samples_num',
            StringField('Number of samples',
                        [validators.Regexp('^[1-9][0-9]*$', message='Digits only.')],
                        default='8'))

    return SamplesheetViewForm


def get_sample_creation_form_class(): #pylint:disable=too-many-statements
    '''
    This function creates the sample creation form:
    1- It first defines the class and its methods.
    2- Then, it adds all attributes to the class. This is done like that because
       of the online retrieval of database information for the select fields.
    3- It returns the class
    '''

    class SampleCreationForm(Form):
        '''
        Class that implements a row of the sample creation table
        (table on the 2nd form on the sample sheet creation workflow).
        '''
        def __validate_sampleno_by_project(self) -> bool:
            '''
            Validate the format of the sample number accordingly with the sample project.
            '''
            sample_number = self.sample_number.data
            project = self.project.data
            project_info = query.get_project_by_name(get_db(), project)

            if project_info['has_glass_no']:
                regex = DEFAULT_GLASSNR
            elif not project_info['has_glass_no'] and project not in PROJECT_SAMPLE_NUMBER_EXCEPTIONS_FORMAT:
                regex = LENIENT
            elif not project_info['has_glass_no'] and project in PROJECT_SAMPLE_NUMBER_EXCEPTIONS_FORMAT:
                regex = PROJECT_SAMPLE_NUMBER_EXCEPTIONS_FORMAT.get(project)

            if not match(regex, sample_number):
                msg = f"Sample no. '{sample_number}' does not pass validation criteria for Project '{project}'"
                self.sample_number.errors.append(msg)
                return False
            return True


        def __validate_patientno_by_project(self) -> bool:
            '''
            Validate the format of the patient number accordingly with the sample project.
            '''
            proj = self.project.data
            pnr = self.patient_number.data
            regex = PROJECT_PATIENTNR_FORMAT.get(proj, DEFAULT_PATIENTNR)
            if match(r"PR[0O]?\d+", proj) and proj not in PROJECT_PATIENTNR_FORMAT:
                # Special case for PRO projects that are not listed in the mapping
                regex = EMPTY
            if not match(regex, pnr):
                msg = f"Patient no. '{pnr}' does not pass validation criteria for Project '{proj}'"
                self.patient_number.errors.append(msg)
                return False
            return True


        def __validate_type_by_project(self) -> bool:
            '''
            Validate the sample type accordingly with the sample project.
            '''
            proj = self.project.data
            stype = self.sample_type.data
            constraint = PROJECT_SAMPLE_TYPE.get(proj, Choices.sample_materials())
            if isinstance(constraint, str) and (not match(constraint, stype)):
                # This case is specific for projects that specify a regex
                # to control the sample type
                msg = f"Type '{stype}' does not pass validation criteria for Project '{proj}'"
                self.sample_type.errors.append(msg)
                return False
            if isinstance(constraint, set) and (stype not in constraint):
                # Here, the sample type does not appear in the list of allowed types
                values = ', '.join(constraint)
                msg = 'Invalid value, must be one of: {values}'.format(values=values)
                self.sample_type.errors.append(msg)
                return False
            return True


        def __validate_library_by_project(self) -> bool:
            '''
            Validate the sample library accordingly with the sample project.
            '''
            proj = self.project.data
            library = self.library.data
            constraint = PROJECT_LIBRARY.get(proj)
            if isinstance(constraint, str) and (not match(constraint, library)):
                # This case is specific for projects that specify a regex
                # to control the library type
                msg = f"Library '{library}' does not pass validation criteria for Project '{proj}'"
                self.library.errors.append(msg)
                return False
            if isinstance(constraint, set) and (library not in constraint):
                # Here, the library does not appear in the list of allowed types
                values = ', '.join(constraint)
                msg = 'Invalid value, must be one of: {values}'.format(values=values)
                self.library.errors.append(msg)
                return False
            return True


        def __validate_patient_generation(self) -> bool:
            '''
            If the project has the attribute has_glass_no set to True, this
            method verifies if the sample has a glassnumber that is registered
            into the labka database.
            '''
            sample_number = self.sample_number.data
            project_info = query.get_project_by_name(get_db(), self.project.data)
            has_glass_no = project_info['has_glass_no']
            if has_glass_no:
                labka_info = get_labka_info_from_glassno(sample_number)
                if not labka_info:
                    msg = 'There is no information in labka for this glass number.'
                    self.sample_number.errors.append(msg)
                    return False
                return self.__validate_external_ids(project_info, labka_info)
            return True


        def __validate_external_ids(self, project_info, labka_info) -> bool:
            '''
            Validate the external identifiers of the sample (from labka)
            For NGC samples, we also verify the library numbers (in case of reruns).

            Arguments
            ---------
            project_info:
                The dictionary that contains the project information (retrieved from the database).
            labka_info:
                The labka dictionary that contains information about the sample (retrieved from labka).

            Return
            ------
            bool
                True if the verification does not find any problem.
            '''
            has_external_ids = project_info['has_external_ids']
            if has_external_ids:
                external_ids = labka_info[const.LABKA_SAMPLE_ALTERNATIVE_NAME]
                if not external_ids:
                    msg = 'This glass number does not have external ids registered in labka. Please, contact the secretaries.'
                    self.sample_number.errors.append(msg)
                    return False
                if project_info['name'] in const.NGC_PROJECTS:
                    return self.__validate_ngc_external_ids_and_libraries(external_ids.strip())
                if project_info['name'] in const.STAGING_PROJECTS:
                    return self.__validate_staging_external_ids(external_ids.strip())
                if project_info['name'] in const.AKUT_PROJECTS:
                    return self.__validate_akut_external_ids(external_ids.strip())
            return True


        def __validate_ngc_external_ids_and_libraries(self, alternative) -> bool:
            '''
            Special validation for NGC samples.
            '''
            # Validating the external ids
            msg = 'This NGC sample does not have the expected number external ids registered in labka. Please, contact the secretaries.'
            alternative = alternative.replace(';', ',')
            ids = alternative.split(',')
            if len(ids) < 2:
                self.sample_number.errors.append(msg)
                return False

            matcher1 = match(const.NGC_REQUISITION_ID_REGEX, ids[0].strip())
            matcher2 = match(const.NGC_SUBJECT_ID_REGEX, ids[1].strip())
            if not matcher1 or not matcher2:
                self.sample_number.errors.append(msg)
                return False

            # Validating the information for creating NGC library numbers
            description = self.description.data
            if not description or 'newlibrary' in description.lower():
                # In this case, we assume that we will have a new library
                return True

            matcher = match(const.FLOWCELL_REGEX, description)
            if not matcher:
                matcher = match(const.MISEQ_FLOWCELL_REGEX, description)
                if not matcher:
                    self.sample_number.errors.append('''
                        If this is a rerun, you must indicate the flowcell of the run where this sample was previously sequenced.
                        Otherwise, Description field must contain the string "newlibrary"
                    ''')
                    return False

            flowcell = matcher.group(1)
            library_id = query.get_library_number(get_db(),
                                                  flowcell,
                                                  self.library.data,
                                                  self.sample_number.data,
                                                  self.index1.data,
                                                  self.index2.data)
            if not library_id:
                self.sample_number.errors.append(''''
                    Could not find the NGC library identifier for this sample.
                    Please, verify the flowcell code in the Description field.
                ''')
                return False

            return True


        def __validate_staging_external_ids(self, alternative) -> bool:
            '''
            Special validation for staging samples.
            '''
            matcher = match(const.STAGING_EXTERNAL_ID_REGEX, alternative)
            if not matcher:
                msg = 'The external id of this sample, registered in labka, does not have the expected format. Please, contact the secretaries.'
                self.sample_number.errors.append(msg)
                return False
            return True


        def __validate_akut_external_ids(self, alternative) -> bool:
            '''
            Special validation for akut samples.
            '''
            matcher = match(const.AKUT_EXTERNAL_ID_REGEX, alternative.strip())
            if not matcher:
                msg = 'The external id of this sample, registered in labka, does not have the expected format. Please, contact the secretaries.'
                self.sample_number.errors.append(msg)
                return False
            return True


        def validate(self):
            '''
            Validate the form data.
            '''
            # First - built-in form validation
            if not Form.validate(self):
                return False

            # Then, custom validation
            # Verifying if the sample already exists
            sample = query.get_sample(get_db(),
                                      self.project.data,
                                      self.patient_number.data,
                                      self.sample_number.data,
                                      self.sample_type.data,
                                      self.library.data,
                                      self.description.data)
            if sample is not None:
                msg = '''Sample seems to already exist! If it is a re-run please provide additional
                         information in the description field.'''
                self.sample_number.errors.append(msg)
                return False

            # Performing custom verification that depends on the project.
            if not self.__validate_sampleno_by_project() or \
                not self.__validate_patientno_by_project() or \
                not self.__validate_type_by_project() or \
                not self.__validate_library_by_project():
                return False

            # Performing custom verification that depends on the
            # existence of a glassnumber in the labka database
            if not self.__validate_patient_generation():
                return False

            # If we arrived here. Hurray!
            return True

    # End of class definition - Begin of attributes definition

    # Field - input text - Lane number
    setattr(SampleCreationForm,
            'lane',
            StringField('Lane on flowcell', [validators.Regexp(r'^[1-8]?$', message='One digit in the range 1-8.')]))

    # Field - input text - Patient number
    setattr(SampleCreationForm,
            'patient_number',
            StringField('Patient specific number, e.g. Fase or Staging number'))

    # Field - input text - Project name
    setattr(SampleCreationForm,
            'project',
            StringField('Project', [ValidateProjectName(should_exist=True)]))

    # Field - input text - Sample number
    setattr(SampleCreationForm,
            'sample_number',
            StringField('Sample number, i.e. glassnumber',
                        [validators.InputRequired()],
                        filters=[lambda x: x.lstrip('0')  if not match(r'[0-9]{4,6}-?[0-9]{4,6}', x) else x,
                                 lambda x: x.replace('-', '') if match(r'[0-9]{4,6}-?[0-9]{4,6}', x) else x,
                                 ## If glassno, remove dash, else replace '-' and '/' with 'x'
                                 lambda x: x.replace('-', 'x').replace('/', 'x'),
                                 lambda x: x.replace('.', '_')]))

    # Field - input text - Sample type
    setattr(SampleCreationForm,
            'sample_type',
            StringField('Sample type', [validators.InputRequired()]))

    # Field - input text - Sample library
    setattr(SampleCreationForm,
            'library',
            StringField('Sample library',
                        [validators.AnyOf(Choices.active_libraries(), message='Invalid value, must be one of: %(values)s')]))

    # Field - input text - Index 1
    setattr(SampleCreationForm,
            'index1',
            StringField('Index 1', [validators.InputRequired()]))

    # Field - input text - Index 2
    setattr(SampleCreationForm,
            'index2',
            StringField('Index 2'))

    # Field - input text - Sample description
    setattr(SampleCreationForm,
            'description',
            StringField('Description'))

    return SampleCreationForm


def get_all_sample_creation_form(n_samples: int):
    '''
    This function creates the all sample creation form (form that groups all individual sample forms):
    The argument defines the minimum and maximum number of expected samples.
    1- It first defines the class and its methods.
    2- Then, it adds all attributes to the class. This is done like that because
       of the online retrieval of database information for the select fields.
    3- It returns the class
    '''

    class AllSampleCreationForm(Form):
        '''
        Class that implements the sample creation form.
        (table on the 2nd form on the sample sheet creation workflow).
        '''
        def __validate_sample_list(self) -> bool:
            '''
            Verifies if we have duplicated samples on the list.

            Return
            ------
            bool
                True if the validation is successfully completed.
            '''
            # Create a list of samples by joining the relevant columns
            samples = ['_'.join((ele['lane'], ele['project'], ele['patient_number'],
                                 ele['sample_number'], ele['sample_type'], ele['library'],
                                 ele['description'])) for ele in self.data['samples']]
            count_samples = Counter(samples)
            duplicate_samples = {x: count_samples[x] for x in count_samples if count_samples[x] > 1}
            if duplicate_samples:
                for sample in duplicate_samples.keys():
                    self.samples.errors.append(f'Sample {sample} exists more than once!')
                    return False
            return True

        def __validate_ngc_sample_material_and_assay(self, assay_name) -> bool:
            '''
            Verifies if the given assay is compatible with NGC samples.
            Verifies if the sample material are compatible with NGC samples.

            Argument
            --------
            assay_name: str
                The assay name.

            Return
            ------
            bool
                True if the validation is successfully completed.
            '''
            # Count the number of NGC samples and validates the sample material.
            ngc_count = 0
            for ele in self.data['samples']:
                if ele['project'] in const.NGC_PROJECTS:
                    ngc_count += 1
                    sample_type = ele['sample_type']
                    material = query.get_sample_material_by_name(get_db(), sample_type)
                    if not material or not material['ngc_name']:
                        self.samples.errors.append(f'The Type {sample_type} is not allowed for NGC samples.')
                        return False

            # Validate the assay
            if ngc_count:
                assay = query.get_assay_by_name(get_db(), assay_name)
                if not assay['ngc_name']:
                    self.samples.errors.append(f'The selected assay ({assay_name}) should not be used for NGC samples.')
                    return False

            return True

        def __validate_indexes_list(self) -> bool:
            '''
            Verifies if we have indexes that are used more than once.

            Return
            ------
            bool
                True if the validation is successfully completed.
            '''
            # Create a list of indexes by joining the relevant columns
            indexes = ['_'.join((ele['lane'], ele['index1'], ele['index2'])) for ele in self.data['samples']]
            count_indexes = Counter(indexes)
            duplicate_indexes = {x: count_indexes[x] for x in count_indexes if count_indexes[x] > 1}
            if duplicate_indexes:
                for index in duplicate_indexes.keys():
                    self.samples.errors.append(f'Index {index} exists more than once!')
                    return False
            return True


        def __validate_indexes_data(self, assay_name) -> bool:
            '''
            Verifies if we have indexes data was correctly entered.

            Argument
            --------
            assay_name: str
                The assay name.

            Return
            ------
            bool
                True if the validation is successfully completed.
            '''
            (index1_set, index2_set) = query.get_index_names_set(get_db(), assay_name)
            index_tuple_list = [(ele['index1'], ele['index2']) for ele in self.data['samples']]
            incompatible_index = False
            for index1, index2 in index_tuple_list:
                if index2_set == {''} and match(r'^[ATCG]+$', index1):
                    # Single index with index1 DNA sequence entered manually
                    continue
                if match(r'^[ATCG]+$', index1) and match(r'^[ATCG]+$', index2):
                    # Double indexes with DNA sequences entered manually
                    continue
                # We have index identifiers, so we check their existence on the database.
                if index1 not in index1_set:
                    msg = f'{index1} index1 does not exist in assay {assay_name}'
                    self.samples.errors.append(msg)
                    incompatible_index = True
                if index2 not in index2_set:
                    msg = f'{index2} index2 does not exist in assay {assay_name}'
                    self.samples.errors.append(msg)
                    incompatible_index = True
                if incompatible_index:
                    return False
            return True


        def validate(self, assay_name: str): # pylint:disable=arguments-differ
            '''
            Validates the form data.

            Argument
            --------
            assay_name: str
                The assay name.
            '''
            # First, built-in validation
            if not Form.validate(self):
                return False

            # Check for samples that are duplicated on the form
            if not self.__validate_sample_list():
                return False

            # Check for duplicated indexes
            if not self.__validate_indexes_list():
                return False

            # Check for incompatible indexes
            if not self.__validate_indexes_data(assay_name):
                return False

            # Check if sample material and assay are ok (for NGC samples)
            if not self.__validate_ngc_sample_material_and_assay(assay_name):
                return False

            return True

    # End of class definition - Begin of attributes definition

    # List of fields - sample forms
    setattr(AllSampleCreationForm,
            'samples',
            FieldList(FormField(get_sample_creation_form_class()), min_entries=n_samples, max_entries=n_samples))

    return AllSampleCreationForm


#########################################################################################
# Auxiliary Class that retrieves data for the form fields (list and sets of values)     #
#########################################################################################

class Choices:
    '''
    This class groups static methods that are used to recover (from the database)
    lists and sets of values that are displayed by the forms or used in
    validation procedures.
    '''
    @staticmethod
    def sequencing_machines(onlyactive: bool) -> List[tuple]:
        '''
        Creates the list of choices for the sequencer select field.

        Argument
        --------
        onlyactive: bool
            If True, only active sequencing machines are added to the list.

        Return
        ------
        List[tuple]
            A list of 2-element tupless containing:
            1 - The sequencing machine code
            2 - The sequencing machine long name
        '''
        choices = [(None, '---')]
        machines = query.get_sequencing_machines(get_db(), onlyactive)
        for machine in machines:
            choices.append((machine['code'], machine['name']))
        return choices


    @staticmethod
    def assays(onlyactive: bool) -> List[tuple]:
        '''
        Creates the list of choices for the assay select field.

        Argument
        --------
        onlyactive: bool
            If True, only active sequencing machines are added to the list.

        Return
        ------
        List[tuple]
            A list of 2-element tuples containing:
            1 - The assay name
            2 - The assay name
           (Observation: the select field use the assay name as value and label)
        '''
        choices = [(None, '---')]
        assays = query.get_assays(get_db(), onlyactive)
        for assay in assays:
            choices.append((assay['name'], assay['name']))
        return choices


    @staticmethod
    def reagentkit(onlyactive: bool) -> List[tuple]:
        '''
        Creates the list of choices for the reagentkit select field.

        Argument
        --------
        onlyactive: bool
            If True, only active sequencing machines are added to the list.

        Return
        ------
        List[tuple]
            A list of 2-element tuples containing:
            1 - The reagent kit version
            2 - The reagent kit version
           (Observation: the select field use the reagent kit version as value and label)
        '''
        # We gather the reagent kit versions, we eliminate duplicates and order them
        choices = set()
        reagentkits = query.get_reagentkits(get_db(), onlyactive)
        for reagentkit in reagentkits:
            choices.add(reagentkit['version'])
        choices = sorted(list(choices))

        # We create the list of tuples needed in the select field
        choices_tuple = [('Default', 'Default')]
        for choice in choices:
            choices_tuple.append((choice, choice))
        return choices_tuple


    @staticmethod
    def active_libraries() -> List[str]:
        '''
        Creates a list of active libraries for the library field validator.

        Return
        ------
        List[str]
            A list of active library names.
        '''
        active = []
        # Retrieve from the database, only active libraries
        libraries = query.get_libraries(get_db(), True)
        for library in libraries:
            active.append(library['name'])
        return active


    @staticmethod
    def sample_materials() -> Set[str]:
        '''
        Retrieves from the database the set of sample materials.

        Return
        ------
        Set[str]
            A set of active sample materials.
        '''
        to_return = set()
        materials = query.get_sample_materials(get_db(), True)
        for material in materials:
            to_return.add(material['name'])
        return to_return
