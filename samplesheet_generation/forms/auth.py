# -*- coding: utf-8 -*-
#
#########################################################################################
# Copyright (C) 2020 GENOMIC MEDICINE - RIGSHOSPITALET                                  #
#########################################################################################
#pylint:disable=too-few-public-methods
'''
Module that contains the classes related with the user registration/authentication forms.

Copyright (C) 2020 GENOMIC MEDICINE - RIGSHOSPITALET

Authors : Bioinformatics team of the Genomic Medicine Department @ RIGSHOSPITALET
- Savvas Kinalis <savvas.kinalis@regionh.dk>
- Mathias Husted Torp <mathias.husted.torp@regionh.dk>
- Filipe Garrett Vieira <filipe.garrett.vieira@regionh.dk>
- Christian Baudet <christian.baudet.01@regionh.dk>
'''
from wtforms import Form, StringField, PasswordField, ValidationError
from werkzeug.security import check_password_hash

from samplesheet_generation.db import get_db

class ValidateUsername():
    '''
    Class for username validation.
    '''
    def __init__(self, throw_error_if_user_exists=True):
        self.throw_error_if_user_exists = throw_error_if_user_exists

    def __call__(self, form, field):
        usermatch = get_db().execute(
            'SELECT id FROM user WHERE username = ?', (field.data,)
        ).fetchone()
        if usermatch is not None and self.throw_error_if_user_exists:
            raise ValidationError('Username {0} is already registered.'.format(field.data))
        elif usermatch is None and not self.throw_error_if_user_exists:
            raise ValidationError('Username {0} is not registered.'.format(field.data))


class ValidatePassword():
    '''
    Class for password validation.
    '''
    def __init__(self, username):
        self.username = username

    def __call__(self, form, field):
        username = form[self.username].data
        userdata = get_db().execute(
            'SELECT password FROM user WHERE username = ?', (username,)
        ).fetchone()
        if userdata and not check_password_hash(userdata['password'], field.data):
            raise ValidationError('Incorrect password for user {0}.'.format(username))
        if not userdata:
            raise ValidationError('')


class ValidateLength():
    '''
    Class for validation of field length.
    '''
    def __init__(self, min=-1, max=-1, message=None):#pylint:disable=redefined-builtin
        self.min = min
        self.max = max
        if not message:
            message = 'Field must be between {0} and {1} characters long.'.format(min, max)
        self.message = message

    def __call__(self, form, field):
        flength = len(field.data) if field.data else 0
        if flength < self.min or self.max != -1 and flength > self.max:
            raise ValidationError(self.message)


class RegistrationForm(Form):
    '''
    Class that represents a user registration form.
    '''
    username = StringField('Username', [
        ValidateLength(min=4, max=25),
        ValidateUsername(throw_error_if_user_exists=True)])
    password = PasswordField('Password', [
        ValidateLength(min=4, max=25)])


class LoginForm(Form):
    '''
    Class that represents a user login form.
    '''
    username = StringField('Username', [
        ValidateUsername(throw_error_if_user_exists=False)])
    password = PasswordField('Password', [
        ValidatePassword(username='username')])
