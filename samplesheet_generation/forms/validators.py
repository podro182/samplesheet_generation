# -*- coding: utf-8 -*-

#########################################################################################
# Copyright (C) 2020 GENOMIC MEDICINE - RIGSHOSPITALET                                  #
#########################################################################################

'''
Module that contains the custom validator classes that are used by the application forms.

Copyright (C) 2020 GENOMIC MEDICINE - RIGSHOSPITALET

Authors : Bioinformatics team of the Genomic Medicine Department @ RIGSHOSPITALET
- Savvas Kinalis <savvas.kinalis@regionh.dk>
- Mathias Husted Torp <mathias.husted.torp@regionh.dk>
- Filipe Garrett Vieira <filipe.garrett.vieira@regionh.dk>
- Christian Baudet <christian.baudet.01@regionh.dk>
'''

#########################################################################################
# Import section                                                                        #
#########################################################################################

from wtforms import ValidationError

from samplesheet_generation.db import get_db, query

#pylint:disable=too-few-public-methods

#########################################################################################
# Validator class section                                                               #
#########################################################################################

class ValidateProjectName():
    '''
    Validator class that is used to check if a given project exists.
    '''
    def __init__(self, should_exist: bool = True):
        '''
        Constructor

        Argument
        --------
        should_exist: bool
            Indicates if the validation should validate the existence of the
            project (True) or the non-existence of the project (False).
        '''
        self.should_exist = should_exist

    def __call__(self, form, field):
        projectmatch = query.get_project_by_name(get_db(), field.data)
        if self.should_exist and projectmatch is None:
            raise ValidationError(f'A project with name {field.data} does not exist.')
        elif not self.should_exist and projectmatch is not None:
            raise ValidationError(f'A project with name {field.data} already exists.')


class ValidateLength():
    '''
    Validator class that is used to validate the lenght of a given string.
    '''
    def __init__(self, min_length: int = -1, max_length: int = -1, message=None):
        '''
        Constructor

        Arguments
        ---------
        min_length: int
            The minimum string length.
        max_lenght: int
            The maximum string length.
        '''
        self.min_length = min_length
        self.max_length = max_length
        if not message:
            # Creating a default error message.
            message = f'Field must be between {min_length} and {max_length} characters long.'
        self.message = message

    def __call__(self, form, field):
        '''
        Method that effectively performs the validation.

        Arguments:
        form
            The related from object.
        field
            The field object that contains the data that is going to be validated.
        '''
        data_length = (len(field.data) if field.data else 0)
        if data_length < self.min_length or \
            (self.max_length != -1 and data_length > self.max_length):
            raise ValidationError(self.message)
