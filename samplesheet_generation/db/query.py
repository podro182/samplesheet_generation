# -*- coding: utf-8 -*-

#########################################################################################
# Copyright (C) 2020 GENOMIC MEDICINE - RIGSHOSPITALET                                  #
#########################################################################################

'''
Module that groups queries on the sample sheet generator database.

Copyright (C) 2020 GENOMIC MEDICINE - RIGSHOSPITALET

Authors : Bioinformatics team of the Genomic Medicine Department @ RIGSHOSPITALET
- Savvas Kinalis <savvas.kinalis@regionh.dk>
- Mathias Husted Torp <mathias.husted.torp@regionh.dk>
- Filipe Garrett Vieira <filipe.garrett.vieira@regionh.dk>
- Christian Baudet <christian.baudet.01@regionh.dk>
'''

#########################################################################################
# Import section                                                                        #
#########################################################################################
from typing import List, Mapping, Set, Tuple
from samplesheet_generation import constants as const

#########################################################################################
# Public functions section                                                              #
#########################################################################################

#########################################################################################
## USER TABLE
def get_user_by_username(db_conn, username: str) -> Mapping[str, object]:
    '''
    Returns the user that has the given username.

    Arguments
    ---------
    db_conn
        The database connection object.
    username: str
        The username of the desired user.

    Return
    ------
    Mapping[str, object]
        The dictionary with the user information.
    '''
    query_str = 'SELECT * FROM user WHERE username = ?'
    return db_conn.execute(query_str, (username,)).fetchone()


def get_user_by_id(db_conn, user_id: int)-> Mapping[str, object]:
    '''
    Returns the user that has the given username.

    Arguments
    ---------
    db_conn
        The database connection object.
    user_id: int
        The username of the desired user.

    Return
    ------
    Mapping[str, object]
        The dictionary with the user information.
    '''
    query_str = 'SELECT * FROM user WHERE id = ?'
    return db_conn.execute(query_str, (user_id,)).fetchone()


#########################################################################################
## SAMPLESHEET TABLE
def get_fullname_of_all_samplesheets(db_conn) -> List[Mapping[str, object]]:
    '''
    Returns the fullname of all sample sheets that are registered into the
    database.

    Argument
    --------
    db_conn
        The database connection object.

    Return
    ------
    List[Mapping[str, object]]
        A list of dictionaries that contains only the attribute fullname.
    '''
    query_str = 'SELECT fullname FROM samplesheet ORDER BY created DESC'
    return db_conn.execute(query_str).fetchall()


def get_samplesheet_by_fullname(db_conn, fullname: str) -> Mapping[str, object]:
    '''
    Retrieves the sample sheet that has the given full name.

    Arguments
    ---------
    db_conn
        The database connection object.
    fullname: str
        The sample sheet full name.

    Return
    ------
    Mapping[str, object]
        A dictionary containing the attributes of the sample sheet that
        has the given full name.
    '''
    query_str = 'SELECT * FROM samplesheet WHERE fullname = ?'
    return db_conn.execute(query_str, (fullname, )).fetchone()


def get_samplesheet_by_id(db_conn, samplesheet_id: int) -> Mapping[str, object]:
    '''
    Retrieves the sample sheet that has the given full name.

    Arguments
    ---------
    db_conn
        The database connection object.
    samplesheet_id: int
        The sample sheet database identifier name.

    Return
    ------
    Mapping[str, object]
        A dictionary containing the attributes of the sample sheet that
        has the given identifier.
    '''
    query_str = 'SELECT * FROM samplesheet WHERE id = ?'
    return db_conn.execute(query_str, (samplesheet_id, )).fetchone()


def get_samplesheet_by_sequencer_run_assay(db_conn,
                                           sequencer: str,
                                           run: str,
                                           assay: str) -> Mapping[str, object]:
    '''
    Retrieves the sample sheet that has the given triplet: sequencer code,
    run number, and assay name.

    Arguments
    ---------
    db_conn
        The database connection object.
    sequencer: str
        The sequencing machine code.
    run: str
        The sequencing run number.
    assay: str
        The assay name.

    Return
    ------
    Mapping[str, object]
        A dictionary containing the attributes of the sample sheet that
        has the given identifier.
    '''
    query_string = '''
        SELECT *
         FROM samplesheet
         WHERE sequencer = ? AND run = ? AND assay = ?
    '''
    return db_conn.execute(query_string, (sequencer, run, assay, )).fetchone()


#########################################################################################
## ASSAY TABLE
def get_assay_by_name(db_conn, assay_name: str) -> Mapping[str, object]:
    '''
    Returns the assay object that is registered into the database
    with the given name.

    Argument
    --------
    db_conn
        The database connection object.
    assay_name: str
        The name of the assay.

    Return
    ------
    Mapping[str, object]
        A dictionary containing the attributes of the assay that has
        the given name.
    '''
    query_str = 'SELECT * FROM assay WHERE name = ?'
    return db_conn.execute(query_str, (assay_name,)).fetchone()


def get_assays(db_conn, only_active: bool) -> List[Mapping[str, object]]:
    '''
    Returns the assay object that is registered into the database
    with the given name.

    Argument
    --------
    db_conn
        The database connection object.
    only_active: bool
        If True, only active assays are returned.

    Return
    ------
    List[Mapping[str, object]]:
        A List of assays information dictionaries.
    '''
    query_str = 'SELECT * FROM assay '
    if only_active:
        query_str += 'WHERE active = 1 '
    query_str += 'ORDER BY name ASC'
    return db_conn.execute(query_str).fetchall()


def get_assay_adapters(db_conn, assay_name: str) -> Tuple[str, str]:
    '''
    Retrieves the DNA adapter sequences for the assay that has the given name.

    Arguments
    ---------
    db_conn
        The database connection object.
    assay_name: str
        The name of the assay.

    Return
    ------
    Tuple[str, str]
        A 2-element tuple with the adapter1 and adapter2 DNA sequences.
    '''

    assay_info = get_assay_by_name(db_conn, assay_name)

    if assay_info['adapter']:
        adapter1 = assay_info['adapter']
        adapter2 = ''
    else:
        adapter1 = assay_info['adapter1']
        adapter2 = assay_info['adapter2']

    return (adapter1, adapter2)


#########################################################################################
## WORD TABLE
def get_word_by_name(db_conn, word_name: str) -> Mapping[str, object]:
    '''
    Returns the words object that is registered into the database
    with the given word.

    Argument
    --------
    db_conn
        The database connection object.
    words_name: str
        The name of the word.

    Return
    ------
    Mapping[str, object]
        A dictionary containing the attributes of the words that has
        the given name.
    '''
    query_str = 'SELECT * FROM word WHERE name = ?'
    return db_conn.execute(query_str, (word_name,)).fetchone()


def get_word_unused(db_conn) -> Mapping[str, object]:
    '''
    Returns the first word object that is registered into the database
    and is not used.

    Argument
    --------
    db_conn
        The database connection object.

    Return
    ------
    Mapping[str, object]
        A dictionary containing the database row of the unused word.
    '''
    query_str = 'SELECT * FROM word WHERE used = ? LIMIT 1'
    return db_conn.execute(query_str, (0,)).fetchone()


#########################################################################################
## PATIENT TABLE
def get_patient_by_name(db_conn, patient_name: str) -> Mapping[str, object]:
    '''
    Returns the patient object that is registered into the database
    with the given patient name.

    Argument
    --------
    db_conn
        The database connection object.
    patient_name: str
        The patient name.

    Return
    ------
    Mapping[str, object]
        A dictionary containing the attributes of the words that has
        the given name.
    '''
    query_str = 'SELECT * FROM patient WHERE name = ?'
    return db_conn.execute(query_str, (patient_name,)).fetchone()


def get_patient_by_cpr(db_conn, patient_cpr: str) -> Mapping[str, object]:
    '''
    Returns the patient object that is registered into the database
    with the given patient name.

    Argument
    --------
    db_conn
        The database connection object.
    patient_cpr: str
        The patient cpr no.

    Return
    ------
    Mapping[str, object]
        A dictionary containing the attributes of the words that has
        the given name.
    '''
    query_str = 'SELECT * FROM patient WHERE cpr_no = ?'
    return db_conn.execute(query_str, (patient_cpr,)).fetchone()


def get_patient_by_id(db_conn, patient_id) -> Mapping[str, object]:
    '''
    Returns the patient object that is registered into the database
    with the given patient id.

    Argument
    --------
    db_conn
        The database connection object.
    patient_id: int
        The patient id.

    Return
    ------
    Mapping[str, object]
        A dictionary containing the attributes of the words that has
        the given name.
    '''
    query_str = 'SELECT * FROM patient WHERE id = ?'
    return db_conn.execute(query_str, (patient_id,)).fetchone()


#########################################################################################
## PROJECT TABLE
def get_project_by_id(db_conn, project_id: int) -> Mapping[str, object]:
    '''
    Returns the project object that is associated with the given database identifier.

    Arguments
    ---------
    db_conn
        The database connection object.
    project_id: int
        The project identifier.

    Return
    ------
    Mapping[str, object]
        A dictionary containing the attributes of the project that has
        the given database identifier.
    '''
    query_str = 'SELECT * FROM project WHERE id = ?'
    return db_conn.execute(query_str, (project_id, )).fetchone()


def get_project_by_name(db_conn, project_name: str) -> Mapping[str, object]:
    '''
    Returns the project object that is associated with the given name.

    Arguments
    ---------
    db_conn
        The database connection object.
    project_name: str
        The project name.

    Return
    ------
    Mapping[str, object]
        A dictionary containing the attributes of the project that has
        the given name.
    '''
    query_str = 'SELECT * FROM project WHERE name = ?'
    return db_conn.execute(query_str, (project_name, )).fetchone()


def get_project_list(db_conn) -> List[Mapping[str, object]]:
    '''
    Returns the list of projects that are registered into the database.

    Arguments
    ---------
    db_conn
        The database connection object.

    Return
    ------
    List[Mapping[str,object]]
        The list of dictionaries with project attributes. The list is sorted
        on ascending order for the project names.
    '''
    return db_conn.execute('SELECT * FROM project ORDER BY name ASC').fetchall()


#########################################################################################
## SAMPLE TABLE
def get_samples_by_samplesheet_id(db_conn, samplesheet_id: int) -> List[Mapping[str, object]]:
    '''
    Returns the list of samples that are associated with the sample sheet that has the
    given database identifier.

    Arguments
    ---------
    db_conn
        The database connection object.
    samplesheet_id: int
        The samplesheet database identifier.

    Return
    ------
    List[Mapping[str,object]]
        The list of dictionaries with attributes of the samples that are associated with
        the given sample sheet database identifier. The list is sorted on asceding order
        for the sample database identifier.
    '''
    query_str = 'SELECT * FROM sample WHERE samplesheet_id = ? ORDER BY id ASC'
    return db_conn.execute(query_str, (samplesheet_id, )).fetchall()


def get_samples(db_conn, project_id: int, #pylint:disable=too-many-arguments
                patient_number: str, sample_number: str,
                sample_type: str, library: str) -> List[Mapping[str, object]]:
    '''
    Returns th list of samples that have the given arguments.

    Arguments
    ---------
    db_conn
        The database connection object.
    project_id: int
        The project database identifier.
    patient_number: str
        The patient number.
    sample_number: str
        The sample number.
    sample_type: str
        The sample type.
    library:
        The library name.

    Return
    ------
    List[Mapping[str,object]]
        The list of dictionaries with attributes of the samples that have the give
        arguments.
    '''
    query_str = '''
        SELECT * FROM sample WHERE
            project_id = ? AND patient_number = ? AND
            sample_number = ? AND type = ? AND library = ?
    '''
    return db_conn.execute(query_str,
                           (project_id, patient_number, sample_number,
                            sample_type, library, )).fetchall()


def get_sample(db_conn, project_name: int, patient_nr: str, #pylint:disable=too-many-arguments
               sample_nr: str, sample_type: str,
               library: str, description: str) -> Mapping[str, object]:
    '''
    Returns the sample is registered into the database and has the given
    attributes.

    Arguments
    ---------
    db_conn
        The database connection object.
    project_id: int
        The project database identifier.
    patient_nr: str
        The patient number.
    sample_nr: str
        The sample number.
    sample_type: str
        The sample type.
    library:
        The library name.
    description:
        The sample description.

    Return
    ------
    Mapping[str, object]
        A dictionary that represents the database row that is related to the
        desired sample.
    '''
    query_string = '''
        SELECT sample.* FROM sample, project WHERE
            project.name = ? AND
            project.id = sample.project_id AND
            sample.patient_number = ? AND
            sample.sample_number = ? AND
            sample.type = ? AND
            sample.library = ? AND
            sample.description = ?
    '''
    return db_conn.execute(query_string,
                           (project_name, patient_nr, sample_nr,
                            sample_type, library, description,)).fetchone()


def get_samples_glass_no(db_conn) -> List[Mapping[str, object]]:
    '''
    Returns th list of samples that have the given arguments.

    Arguments
    ---------
    db_conn
        The database connection object.

    Return
    ------
    List[Mapping[str,object]]
        The list of dictionaries with attributes of the samples that have the give
        arguments.
    '''
    query_str = '''
        SELECT id, sample_number
         FROM sample
    '''
    return db_conn.execute(query_str).fetchall()


def get_ngc_samples_by_samplesheet_id(db_conn, samplesheet_id: int) -> List[Mapping[str, object]]:
    '''
    Returns the list of NGC samples that are associated with the sample sheet that has the
    given database identifier.

    Arguments
    ---------
    db_conn
        The database connection object.
    samplesheet_id: int
        The samplesheet database identifier.

    Return
    ------
    List[Mapping[str,object]]
        The list of dictionaries with attributes of the NGC samples that are associated with
        the given sample sheet database identifier. The list is sorted on asceding order
        for the sample database identifier.
    '''
    parameters = [samplesheet_id]
    parameters.extend(const.NGC_PROJECTS)
    aux = ','.join(['?'] * len(const.NGC_PROJECTS))
    query_str = f'''
        SELECT s.* FROM sample s, project p
        WHERE samplesheet_id = ? AND s.project_id=p.id AND p.name IN ({aux})
        ORDER BY id ASC
    '''
    return db_conn.execute(query_str, parameters).fetchall()


#########################################################################################
## INDEX1, INDEX2, AND INDEXDOUBLE TABLES
def get_index_set(db_conn, assay_id: int, indextype: str,
                  indexnr: int = None, novaseqbases: bool = True) -> Mapping[str, str]:
    '''
    Retrieve the desired set of indexes.

    Arguments
    ---------
    db_conn
        The database connection object.
    assay_id: int
        The database identifier of the assay.
    indextype: str
        The type of index that should be recovered.
    indexnr: int
        The number of the index that should be retrieved (1 or 2).
    novaseqbases: bool
        For the index2, this flag indicates if we should recover
        the index DNA sequence for NovaSeq style sequencers (True)
        or NextSeq style (False).

    Return
    ------
    Mapping[str, str]
        A dictionary containing index names (key) and index sequences (value).

    Raise
    -----
    ValueError
        When we have an invalid set of arguments.
    '''
    column_name = 'bases'
    if indextype == 'singleIndex' and indexnr == 1:
        query_str = 'SELECT * FROM index1 WHERE assay_id = ?'
    elif indextype == 'doubleIndex' and indexnr == 1:
        query_str = 'SELECT * FROM index1 WHERE assay_id = ?'
    elif indextype == 'doubleIndex' and indexnr == 2:
        query_str = 'SELECT * FROM index2 WHERE assay_id = ?'
        if novaseqbases:
            column_name = 'novabases'
        else:
            column_name = 'nextbases'
    elif indextype == 'uniqueDI' and indexnr == 1:
        query_str = 'SELECT * FROM indexDouble WHERE assay_id = ?'
        column_name = 'i7bases'
    elif indextype == 'uniqueDI' and indexnr == 2:
        query_str = 'SELECT * FROM indexDouble WHERE assay_id = ?'
        if novaseqbases:
            column_name = 'i5novabases'
        else:
            column_name = 'i5nextbases'
    else:
        raise ValueError('Invalid set of arguments.')

    indexes = db_conn.execute(query_str, (assay_id, )).fetchall()
    indexes = {ele['name'] : ele[column_name] for ele in indexes}
    return indexes


def get_index_names_set(db_conn, assay_name: str) -> Tuple[Set, Set]:
    '''
    Function tha retrieves the set of index names that are associated with
    the assay that has the given name.

    Arguments
    ---------
    db_conn
        The database connection object.
    assay_name: str
        The name of the desired assay.

    Return
    ------
    Tuple[Set, Set]
        A 2-element tuple containing 2 sets of index names (index1 and index2)
        that are associated with the given assay.
    '''

    assay_info = get_assay_by_name(db_conn, assay_name)
    index1 = {''}
    index2 = {''}
    if assay_info['singleIndex']:
        index1 = __retrieve_index_name_set(db_conn, assay_info['id'], index=1)
    elif assay_info['doubleIndex']:
        index1 = __retrieve_index_name_set(db_conn, assay_info['id'], index=1)
        index2 = __retrieve_index_name_set(db_conn, assay_info['id'], index=2)
    elif assay_info['uniqueDI']:
        index1 = __retrieve_index_name_set(db_conn, assay_info['id'], index=3)

    return (index1, index2)


def __retrieve_index_name_set(db_conn, assay_id: int, index: int = 1):
    '''
    Auxiliary function that returns the set of index names that is
    associated with assay that has the given identifier.

    Arguments
    ---------
    db_conn
        The database connection object.
    assay_id: int
        The assay identifier.
    index: int
        One of the following values:
        1 : retrieve from index1 table;
        2 : retrieve from index2 table;
        3 : retrieve from indexDouble table.
    '''
    if index not in (1, 2, 3):
        raise ValueError(f'Invalid index number : {index}')
    if index in (1, 2):
        query = f'SELECT name FROM index{index} WHERE assay_id = ?'
    else:
        query = f'SELECT name FROM indexDouble WHERE assay_id = ?'
    indexes = db_conn.execute(query, (assay_id, )).fetchall()
    indexes = {ele['name'] for ele in indexes}
    return indexes


#########################################################################################
## SEQMACHINE TABLE
def get_sequencing_machines(db_conn,
                            only_active: bool) -> List[Mapping[str, object]]:
    '''
    Retrive the list of sequencing machines that are registered into the database.

    Arguments
    ---------
    db_conn
        The database connection object.
    only_active: bool
        If True, only active machines are returned.

    Return
    ------
    List[Mapping[str, object]]:
        The list of sequencing machine dictionaries.
    '''
    query_str = 'SELECT * FROM seqmachine '
    if only_active:
        query_str += 'WHERE active = 1 '
    query_str += 'ORDER BY code ASC'
    return db_conn.execute(query_str).fetchall()


def get_sequencing_machine_by_code(db_conn,
                                   code: str) -> Mapping[str, object]:
    '''
    Retrieve the sequencing machine that is identified by the given code.

    Arguments
    ---------
    db_conn
        The database connection object.
    code: str
        The sequencing machine code

    Return
    ------
    Mapping[str, object]:
        The dictionary with the sequencing machine data.
    '''
    query_str = 'SELECT * FROM seqmachine WHERE code = ?'
    return db_conn.execute(query_str, (code,)).fetchone()


#########################################################################################
## reagentkit TABLE
def get_reagentkits(db_conn,
                  only_active: bool) -> List[Mapping[str, object]]:
    '''
    Retrieve the list of reagent kits that are registered into the database.

    Arguments
    ---------
    db_conn
        The database connection object.
    only_active: bool
        If True, only active libraries are returned.

    Return
    ------
    List[Mapping[str, object]]:
        The list of library dictionaries.
    '''
    query_str = 'SELECT * FROM reagentkit'
    if only_active:
        query_str += ' WHERE active = 1'
    return db_conn.execute(query_str).fetchall()


def get_novaseqbases_by_model_reagentkit(db_conn,
                                         seqmodel: str,
                                         reagentkit_version: str) -> int:
    '''
    Retrieve the indexes that should be used based on the sequencing machine model and 
    the reagent kit.

    Arguments
    ---------
    db_conn
        The database connection object.
    seqmodel: str
        The sequencing machine model.
    reagentkit_version: str
        The reagent kit version.
    
    Return
    ------
    int
        The identifier that indicates if novaseqbases (1) or the nextseqbases (0) 
        are to be used
    '''
    query_str = 'SELECT * FROM reagentkit WHERE seqmodel=? AND version=?'
    data = db_conn.execute(query_str,
                           (seqmodel, reagentkit_version,)).fetchone()
    if data:
        return data['novaseqbases']
    else:
        return None


def get_reagentkit_default_by_seqmachine(db_conn, seqmodel: str) -> str:
    '''
    Retrieve the version of the default reagent kit for specified sequencing machine model.

    Arguments
    ---------
    db_conn
        The database connection object.
    seqmodel: str
        The sequencing machine model.

    Return
    ------
    str
        The reagent kit version.
    '''
    query_str = 'SELECT * FROM reagentkit WHERE seqmodel=? AND is_default=?'
    data = db_conn.execute(query_str,
                           (seqmodel, '1',)).fetchone()
    if data:
        return data['version']
    else:
        return None
        

#########################################################################################
## Library TABLE
def get_libraries(db_conn,
                  only_active: bool) -> List[Mapping[str, object]]:
    '''
    Retrieve the list of libraries that are registered into the database.

    Arguments
    ---------
    db_conn
        The database connection object.
    only_active: bool
        If True, only active libraries are returned.

    Return
    ------
    List[Mapping[str, object]]:
        The list of library dictionaries.
    '''
    query_str = 'SELECT * FROM library '
    if only_active:
        query_str += 'WHERE active = 1 '
    query_str += 'ORDER BY name,version ASC'
    return db_conn.execute(query_str).fetchall()


def get_active_library_by_name(db_conn,
                               name: str) -> Mapping[str, object]:
    '''
    Retrive the library that is identified by the given name and it is
    active. IMPORTANT: this method expects that only one library with
    the given name is active.

    Arguments
    ---------
    db_conn
        The database connection object.
    name: str
        The library name

    Return
    ------
    Mapping[str, object]:
        The dictionary with the library data.
    '''
    query_str = 'SELECT * FROM library WHERE name = ? AND active = 1'
    return db_conn.execute(query_str, (name,)).fetchone()


#########################################################################################
## SAMPLE_MATERIAL TABLE
def get_sample_materials(db_conn,
                         only_active: bool) -> List[Mapping[str, object]]:
    '''
    Retrive the list of sample materials that are registered into the database.

    Arguments
    ---------
    db_conn
        The database connection object.
    only_active: bool
        If True, only active sample materials are returned.

    Return
    ------
    List[Mapping[str, object]]:
        The list of sample material dictionaries.
    '''
    query_str = 'SELECT * FROM sample_material '
    if only_active:
        query_str += 'WHERE active = 1 '
    query_str += 'ORDER BY name ASC'
    return db_conn.execute(query_str).fetchall()


def get_sample_material_by_name(db_conn,
                                name: str) -> Mapping[str, object]:
    '''
    Retrive the sample material that is identified by the given name.

    Arguments
    ---------
    db_conn
        The database connection object.
    name: str
        The library name
    version: str
        The library version

    Return
    ------
    Mapping[str, object]:
        The dictionary with the library data.
    '''
    query_str = 'SELECT * FROM sample_material WHERE name = ?'
    return db_conn.execute(query_str, (name,)).fetchone()


#########################################################################################
## NGC_LIBRARY TABLE
def get_next_library_number(db_conn, library: str) -> int:
    '''
    Retrive the maximum NGC library ID that is identified by the given name.

    Arguments
    ---------
    db_conn
        The database connection object.
    library: str
        The library name

    Return
    ------
    int
        The next available library identifier.
    '''
    query_str = 'SELECT max(library_number) AS max FROM ngc_library WHERE library_type = ?'
    data = db_conn.execute(query_str, (library,)).fetchone()
    next_id = 0
    if data and data['max']:
        next_id = data['max'] + 1
    if library == 'WGS' and next_id < const.NGC_WGS_INITIAL_LIBRARY_ID:
        return const.NGC_WGS_INITIAL_LIBRARY_ID
    if library == 'IDSNP' and next_id < const.NGC_IDSNP_INITIAL_LIBRARY_ID:
        return const.NGC_IDSNP_INITIAL_LIBRARY_ID
    return next_id


def get_library_number(db_conn, flowcell: str, library: str, #pylint:disable=too-many-arguments
                       glassnumber: str, index1: str, index2: str) -> int:
    '''
    Retrive the NGC library ID for the sample that has been already sequenced.

    Arguments
    ---------
    db_conn
        The database connection object.
    flowcell: str
        The sequencing run flowcell.
    library: str
        The library name.
    glassnumber:str
        The sample glassnumber.
    index1: str
        Name of the index1.
    index2: str
        Name of the index2.

    Return
    ------
    int
        The id that was assigned to the sample in the given sequencing run.
    '''
    query_str = '''
        SELECT library_number FROM
            samplesheet AS A, sample AS B, ngc_library C
        WHERE
            A.id=B.samplesheet_id AND B.id=C.sample_id AND
            A.flowcell=? AND B.sample_number=? AND B.library=? AND
            B.index1=? AND B.index2=? AND C.library_type=?
    '''
    data = db_conn.execute(query_str,
                           (flowcell, glassnumber, library, index1, index2, library,)).fetchone()
    if data:
        return data['library_number']
    return None


def get_sample_library_number(db_conn, sample_id: int) -> int:
    '''
    Retrive the NGC library ID for the sample that has the given identifier.

    Arguments
    ---------
    db_conn
        The database connection object.
    sample_id: int
        The sample database identifier.

    Return
    ------
    int
        The library number.
    '''
    query_str = 'SELECT library_number FROM ngc_library WHERE sample_id = ?'
    data = db_conn.execute(query_str, (sample_id,)).fetchone()
    if data:
        return data['library_number']
    return None
