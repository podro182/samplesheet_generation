# -*- coding: utf-8 -*-
#pylint:disable=c-extension-no-member

#########################################################################################
# Copyright (C) 2020 GENOMIC MEDICINE - RIGSHOSPITALET                                  #
#########################################################################################

'''
Module that provides functions for database interaction.

Copyright (C) 2020 GENOMIC MEDICINE - RIGSHOSPITALET

Authors : Bioinformatics team of the Genomic Medicine Department @ RIGSHOSPITALET
- Savvas Kinalis <savvas.kinalis@regionh.dk>
- Mathias Husted Torp <mathias.husted.torp@regionh.dk>
- Filipe Garrett Vieira <filipe.garrett.vieira@regionh.dk>
- Christian Baudet <christian.baudet.01@regionh.dk>
'''

#########################################################################################
# Imports section                                                                       #
#########################################################################################
import sys
import json
import sqlite3
from multiprocessing.pool import worker
from pathlib import Path

import click

from flask import current_app, g
from flask.cli import with_appcontext

from samplesheet_generation import constants
from samplesheet_generation.db import query, insert_update
from samplesheet_generation.labka_lookup import get_labka_info_from_glassno
import cx_Oracle

#########################################################################################
# Public functions section                                                              #
#########################################################################################

# Function that creates a connection with the database and returns a database connection
# handler
def get_db(raise_exception: bool = True):
    '''
    Returns a database connection handler.

    Argument
    --------
    raise_exception: bool
        If True, raise an exception if the database file is missing.
    db_path: Path
        If not None, use the given database file path. Otherwise,
        recovers the path from the application configuration.

    Return
    ------
    A sqlite3 database connection handler.

    Raise
    -----
    RuntimeError
        When the flag raise_exception is True and the database file does
        not exist.
    '''
    if 'db' not in g:
        if raise_exception:
            if constants.CONFIG_DB_FILE not in current_app.config:
                raise RuntimeError(f'Incomplete configuration: missing database file name.')
            if not current_app.config[constants.CONFIG_DB_FILE]:
                raise RuntimeError(f'Incomplete configuration: missing database file name.')
        dbfile_path = Path(current_app.config[constants.CONFIG_DB_FILE])
        g.db = create_database_connection(raise_exception=raise_exception,
                                          db_path=dbfile_path)
    return g.db


def create_database_connection(raise_exception: bool = True, db_path: Path = None):
    '''
    Returns a database connection handler.

    WARNING!!!
    YOU SHOULD NOT DIRECTLY USE THIS METHOD INSIDE OF THE APPLICATION CONTEXT.
    UNDER THE APPLICATION CONTEXT, USE THE METHOD get_db()

    Argument
    --------
    raise_exception: bool
        If True, raise an exception if the database file is missing.
    db_path: Path
        If not None, use the given database file path. Otherwise,
        recovers the path from the application configuration.

    Raise
    -----
    RuntimeError
        When the flag raise_exception is True and the database file does
        not exist.
    '''
    if raise_exception and not db_path.exists():
        raise RuntimeError(f'Could not find the file {db_path}.')
    db_conn = sqlite3.connect(str(db_path),
                              detect_types=sqlite3.PARSE_DECLTYPES)
    db_conn.row_factory = sqlite3.Row
    return db_conn



# Function that closes the connection with the database
def close_db(e=None) -> None: #pylint:disable=invalid-name,unused-argument
    '''
    Closes the connection with the database.
    '''
    db_handler = g.pop('db', None)
    if db_handler is not None:
        db_handler.close()


# Function that initialises the application by adding
# the close_db method in the tear down context.
def init_app(app) -> None:
    '''
    Inits the application by adding the close_db function
    to the application context.
    '''
    app.teardown_appcontext(close_db)


# Function that creates an empty database
def create_empty_database(db_conn, base_dir: Path) -> None:
    '''
    This function is used to create a completelly empty database.

    Argument
    --------
    db_conn
        The database connection.
    base_dir: Path
        The path for the base directory of the application (parent folder of
        the samplesheet_generation module folder).
    '''
    print('Creating an empty database.', file=sys.stderr)

    # Creating the path for the SQL script that creates an empty schema
    sql_script = base_dir.joinpath('samplesheet_generation', 'sql', 'schema.sql')

    # Executing the script that drop and recreate the tables.
    with open(sql_script, 'rb') as sqlcommands:
        db_conn.executescript(sqlcommands.read().decode('utf8'))
        db_conn.commit()


# Function that initialises the assays and indexes tables
def init_assays_and_indexes_tables(db_conn, base_dir: Path) -> None:
    '''
    This function drops, recreates and populates the assays and
    indexes tables.

    Argument
    --------
    db_conn
        The database connection.
    base_dir: Path
        The path for the base directory of the application (parent folder of
        the samplesheet_generation module folder).
    '''
    print('Updating assays and indexes tables.', file=sys.stderr)

    # Creating the path for the initialize assay db script
    sql_script = base_dir.joinpath('samplesheet_generation', 'sql', 'initialize_assay_db.sql')

    # Executing the script that drop and recreate the tables.
    with open(sql_script, 'rb') as sqlcommands:
        db_conn.executescript(sqlcommands.read().decode('utf8'))

    # Inserting the data into the tables
    __populates_assay_and_indexes(db_conn, base_dir)

    # Committing all the operation
    db_conn.commit()


# Function that initialises the assays and indexes tables
def init_basic_tables(db_conn, base_dir: Path) -> None:
    '''
    This function drops, recreates and populates the assays and
    indexes tables.

    Argument
    --------
    db_conn
        The database connection.
    base_dir: Path
        The path for the base directory of the application (parent folder of
        the samplesheet_generation module folder).
    '''
    print('Creating the basic tables and inserting values on them.', file=sys.stderr)

    # List of SQL scripts to process
    filelist = ['initialize_library_table.sql',
                'initialize_sample_material_table.sql',
                'initialize_seqmachine_table.sql']

    for filename in filelist:
        print(f'Processing {filename}.', file=sys.stderr)
        sql_script = base_dir.joinpath('samplesheet_generation', 'sql', filename)
        with open(sql_script, 'rb') as sqlcommands:
            db_conn.executescript(sqlcommands.read().decode('utf8'))

    # Committing all the operation
    db_conn.commit()


# Function that loads the json file and populates the assays and indexes tables
def __populates_assay_and_indexes(db_conn, base_dir: Path) -> None:
    '''
    This function loads the assays JSON file and insert the data into the
    database.

    Argument
    --------
    db_conn
        The database connection.
    base_dir: Path
        The path for the base directory of the application (parent folder of
        the samplesheet_generation module folder).
    '''
    # Creating the path for the assay JSON file
    json_file = base_dir.joinpath('samplesheet_generation', 'resources', 'assays.json')

    # Loading the JSON file
    with open(json_file, 'r') as file_handler:
        index_data = json.load(file_handler)

    # Inserting each one of the assays
    for assay in index_data:
        assay_info = query.get_assay_by_name(db_conn, assay['assay'])
        if assay_info is None:
            insert_update.insert_assay(db_conn, assay)


# Function that initialises the word tables
def init_word(db_conn, base_dir: Path) -> None:
    '''
    This function populates the word table.

    Argument
    --------
    db_conn
        The database connection.
    base_dir: Path
        The path for the base directory of the application (parent folder of
        the samplesheet_generation module folder).
    '''
    print('Populating word table.', file=sys.stderr)

    # Inserting the already generated data into the tables
    __populate_word(db_conn, base_dir)

    # Committing the rest of the operation
    db_conn.commit()


# Function that loads the data files and populates the word table
def __populate_word(db_conn, base_dir: Path) -> None:
    '''
    This function loads the text file with the pronounceable words and
    inserts the data into the database.

    Argument
    --------
    db_conn
        The database connection.
    base_dir: Path
        The path for the base directory of the application (parent folder of
        the samplesheet_generation module folder).
    '''

    # Insert the words from the file in the database if they are not already inserted
    words_file = base_dir.joinpath('samplesheet_generation', 'resources', 'words.txt')

    with open(words_file, 'r') as file_handler:

        # Initialize counter for the commits
        counter = 0

        # Read words from the file
        for word in file_handler.readlines():
            word = word.strip()
            # word_info = query.get_word_by_name(db_conn, word)
            # if word_info is None:
            insert_update.insert_word(db_conn, word)

            # Commit every once in a while
            counter += 1
            if counter > 1000:
                db_conn.commit()
                counter = 0


# Function that loads data files and populates the patient table
# This function will populate the patients that have been manually assigned a patient_id
# by Tobias.
def init_patient(db_conn, patients_csv) -> None: #pylint:disable=too-many-locals,too-many-branches
    '''
    This function loads the already assigned patient ids and inserts the data into the database.

    Argument
    --------
    db_conn
        The database connection.
    base_dir: Path
        The path for the base directory of the application (parent folder of
        the samplesheet_generation module folder).
    '''
    try:
        _ = get_labka_info_from_glassno('123456789123')
    except cx_Oracle.DatabaseError as exception:
        print('Cannot connect to labka database.')
        print('The following error occurs:')
        print(exception)
        raise

    print('Populating database with the patient ids provided.')
    # Insert the patient entries in the database if they are not already inserted.
    with open(patients_csv, 'r', encoding='utf-8') as file_handler:
        # Initialize counter for the commits
        counter = 0

        # Read patients from the file
        for patient in file_handler.readlines():

            try:
                word, cpr_no, name = patient.split(',')
                name = name.strip()
            except ValueError:
                print("The file {} should be a csv with 3 columns: "
                      "word, cpr_no, (patient) name".format(str(patients_csv)))
                raise

            patient_info = query.get_patient_by_name(db_conn, word)
            if patient_info is None:

                word_info = query.get_word_by_name(db_conn, word)
                if not word_info:
                    insert_update.insert_word(db_conn, word)
                    word_info = query.get_word_by_name(db_conn, word)

                if not word_info['used']:
                    word_id = word_info['id']
                    insert_update.update_word_as_used(db_conn, word_id)
                    insert_update.insert_patient(db_conn, word_id, cpr_no, name)

                    # Commit every once in a while
                    counter += 1
                    if counter > 1000:
                        db_conn.commit()
                        counter = 0
                else:
                    print('Omitted {} cause it was currently in use.'.format(name))

    db_conn.commit()

    print('Assigning samples from the DB to patient IDs.')

    # Get all patient numbers (aka glass numbers) from the database.
    sample = dict()
    samples_glass_no = query.get_samples_glass_no(db_conn)
    for sample_info in samples_glass_no:
        sample['id'], sample['glass_no'] = sample_info

        # Naively check if the patient number is a glass number
        if sample['glass_no'] and len(sample['glass_no']) == 12:

            # Get labka info for the glass number
            labka_info = get_labka_info_from_glassno(sample['glass_no'])

            # Check if we have CPR for that sample
            if labka_info:
                cpr_no = labka_info['CPR'].replace("-", "")

                # Find corresponding patient entry in the database
                patient_info = query.get_patient_by_cpr(db_conn, cpr_no)

                # If there is a patient entry corresponding to the sample entry,
                # link them
                if patient_info:
                    insert_update.update_sample_with_patient_id(db_conn,
                                                                sample['id'],
                                                                patient_info['id'])

                    # Commit every once in a while
                    counter += 1
                    if counter > 1000:
                        db_conn.commit()
                        counter = 0
