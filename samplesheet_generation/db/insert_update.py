# -*- coding: utf-8 -*-

#########################################################################################
# Copyright (C) 2020 GENOMIC MEDICINE - RIGSHOSPITALET                                  #
#########################################################################################

'''
Module that groups insertion and update operations on the sample sheet
generator database.

Copyright (C) 2020 GENOMIC MEDICINE - RIGSHOSPITALET

Authors : Bioinformatics team of the Genomic Medicine Department @ RIGSHOSPITALET
- Savvas Kinalis <savvas.kinalis@regionh.dk>
- Mathias Husted Torp <mathias.husted.torp@regionh.dk>
- Filipe Garrett Vieira <filipe.garrett.vieira@regionh.dk>
- Christian Baudet <christian.baudet.01@regionh.dk>
'''

#########################################################################################
# Imports section                                                                       #
#########################################################################################

from typing import Mapping

from samplesheet_generation.db import query

#pylint:disable=too-many-arguments
#pylint:disable=too-many-locals
#pylint:disable=line-too-long

#########################################################################################
# Public functions section                                                              #
#########################################################################################

def insert_samplesheet(db_conn, fullname: str, author: int,
                       sequencer: str, run: str, flowcell: str, iemfileversion: str,
                       date: str, workflow: str, application: str, assay: str,
                       reagentkit: str, description: str, chemistry: str, reads1: str,
                       reads2: str, umi: str, adapter1: str, adapter2: str
                       ) -> Mapping[str, object]:
    '''
    Inserts a sample sheet entry into the samplesheet table.

    Important: the method does not commit the operation. You must do it by yourself.

    Arguments
    ---------
    db_conn
        The database connection object.
    fullname: str
        The sample sheet full name.
    author: int
        The identifier of the person that is creating the sample sheet.
    sequencer: str
        The sequencing machine that will be used for the sequencing run.
    run: str
        The "estimated" run number (only for visualisation purposes).
    flowcell: str
        The flowcell that will be used in the sequencing run.
    iemfileversion: str
        The version of the IEM file.
    date: str
        The sequencing run date.
    workflow: str
        The workflow that is associated with the sample sheet.
    application: str
        The application that is associated with the sample sheet.
    assay: str
        The assay that was used for preparing the samples.
    reagentkit: str
        The reagent kit version used in the sequencing machine.
    description: str
        Some description about the sample sheet.
    chemistry: str
        The chemistry that was used for preparing the samples.
    reads1: str
        The length of the read1.
    reads2: str
        The length of the read2
    umi: str
        The lenght of the umi.
    adapter1: str
        The sequence of the adapter 1.
    adapter2: str
        The sequence of the adapter 2.

    Return
    ------
    Mapping[str, object]:
        The samplesheet database row that was just inserted into the database.
    '''

    insert_string = '''
        INSERT INTO samplesheet
         (fullname, author_id, sequencer, run, flowcell, iemfileversion, date, workflow,
          application, assay, reagentkit, description, chemistry, reads1, reads2, umi,
          adapter1, adapter2)
         VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
        '''

    db_conn.execute(insert_string,
                    (fullname, author, sequencer, run, flowcell,
                     iemfileversion, date, workflow, application,
                     assay, reagentkit, description, chemistry,
                     reads1, reads2, umi, adapter1, adapter2))

    return query.get_samplesheet_by_fullname(db_conn, fullname)


def insert_sample(db_conn, samplesheet_info,
                  sample_position: int,
                  patient_number: str,
                  sample_lane: int,
                  sample_number, sample_type,
                  sample_library, sample_project,
                  index1, index2, description,
                  patient_id, external_ids) -> Mapping[str, object]:
    '''
    Inserts a sample sheet into the database.

    Important: the method does not commit the operation. You must do it by yourself.

    Arguments
    ---------
    db_conn
        The database connection object.
    samplesheet_info:
        The object that contains the database information of the sample sheet.
    patient_number: str
        The number of the patient that is associated with the sample.
    sample_position: int
        The position of the sample in the list.
    sample_lane: int
        The lane where the sample is being sequenced. If None, we assume that
        tge sample appears in all lanes.
    sample_number: str
        The sample number (glaslls number, in most cases)
    sample_type: str
        The type of the sample.
    sample_library: str
        The sample library type.
    sample_project:
        The sample project.
    index1
        The name or DNA sequence of the index1.
    index2
        The name or DNA sequence of the index2.
    description
        Some extra description about the sample.
    patient_id
        The id of the patient table entry that corresponds to this sample.
    external_ids
        A string containing a list of external identifiers separated by semicolon.

    Return
    ------
    Mapping[str, object]:
        The sample database row that was just inserted into the database.
    '''
    project_info = query.get_project_by_name(db_conn, sample_project)
    sample_uid = __create_sample_uid(db_conn, samplesheet_info, sample_position)
    insert_string = '''
        INSERT INTO sample
         (lane, sample_uid, patient_number, project_id, sample_number,
         type, library, index1, index2, description, samplesheet_id, patient_id, external_ids)
         VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
    '''
    db_conn.execute(insert_string,
                    (sample_lane, sample_uid, patient_number, project_info['id'],
                     sample_number, sample_type, sample_library,
                     index1, index2, description, samplesheet_info['id'], patient_id,
                     external_ids))
    return query.get_sample(db_conn, sample_project, patient_number,
                            sample_number, sample_type, sample_library, description)


def update_sample_with_patient_id(db_conn, sample_id: int, patient_id: int) -> None:
    '''
    Updates a sample that can be associated with a patient id.

    Important: the method does not commit the operation. You must do it by yourself.

    Arguments
    ---------
    db_conn
        The database connection object.
    sample_id
        The id of the sample table entry that corresponds to this sample.
    patient_id
        The id of the patient table entry that corresponds to this sample.
    '''
    insert_string = '''
          UPDATE sample
          SET patient_id = ?
          WHERE id = ?
        '''
    db_conn.execute(insert_string,
                    (patient_id, sample_id))


def insert_patient(db_conn, word_id, cpr_no, name):
    '''
    Inserts a row in the patient table.

    Important: the method does not commit the operation. You must do it by yourself.

    :param db_conn:
        The database connection object.
    :param word_id:
        The integer id of the row of table word of the database.
    :param cpr_no:
        The integer CPR no that is uniquely mapped to the name of the patient.
    :param name:
        Name of the patient generated.
    :return:
        The database identifier of the new patient row.
    '''
    insert_string = '''
        INSERT INTO patient
         (word_id, cpr_no, name)
         VALUES (?, ?, ?)
        '''
    db_conn.execute(insert_string,
                    (word_id, cpr_no, name))

    return query.get_patient_by_name(db_conn, name)


def update_word_as_used(db_conn, word_id) -> None:
    '''
    Sets word as used in the word table.

    Important: the method does not commit the operation. You must do it by yourself.

    :param db_conn:
        The database connection object.
    :param word_id:
        The integer id of the row of table word of the database.
    '''
    update_string = '''
        UPDATE word SET used = ?
         WHERE id = ?
    '''
    db_conn.execute(update_string,
                    (1, word_id))


def insert_project(db_conn,
                   author_id: int,
                   project_name: str,
                   internal_project: bool,
                   has_glass_no: bool,
                   project_contact_name: str,
                   project_contact_email: str,
                   project_description: str) -> None:
    '''
    Inserts a new project into the database.

    Important: the method does not commit the operation. You must do it by yourself.

    Arguments
    ---------
    db_conn
        The database connection object.
    author_id: int
        The database identifier of the user that is creating the project.
    project_name: str
        The name of the new project
    internal_project: bool
        Flag that indicates if the project is an internal project (True) or
        an external project (False).
    has_glass_no: bool
        Flag that indicates if the project samples will get a glass number
        (True) or not (False)
    project_contact_name: str
        The name of the person that is responsible for this new project.
    project_contact_email: str
        The email of contact.
    project_description: str
        The project description.
    '''
    insert_string = '''
        INSERT INTO project (author_id, name, internal, has_glass_no, contact, contact_mail, description)
         VALUES (?, ?, ?, ?, ?, ?, ?)
    '''
    db_conn.execute(insert_string,
                    (author_id, project_name, internal_project, has_glass_no, project_contact_name,
                     project_contact_email, project_description))


def update_project(db_conn,
                   project_name: str,
                   internal_project: bool,
                   has_glass_no: bool,
                   project_contact_name: str,
                   project_contact_email: str,
                   project_description: str) -> None:
    '''
    Updates the details of a project that is registered into the database.

    Important: the method does not commit the operation. You must do it by yourself.

    Arguments
    ---------
    db_conn
        The database connection object.
    project_name: str
        The name of the project that will be updated.
    internal_project: bool
        Flag that indicates if the project is an internal project (True) or
        an external project (False).
    has_glass_no: bool
        Flag that indicates if the project samples will get a glass number
        (True) or not (False)
    project_contact_name: str
        The name of the person that is responsible for this new project.
    project_contact_email: str
        The email of contact.
    project_description: str
        The project description.
    '''
    update_string = '''
        UPDATE project SET internal = ?, has_glass_no = ?, contact = ?, contact_mail = ?, description = ?
         WHERE name = ?
    '''
    db_conn.execute(update_string,
                    (internal_project, has_glass_no, project_contact_name, project_contact_email,
                     project_description, project_name))


def insert_assay(db_conn, assay_data: Mapping[str, object]) -> int:
    '''
    Insert the assay data into the database.

    Important: the method does not commit the operation. You must do it by yourself.

    Arguments
    ---------
    db_conn
        The database connection object.
    assay_data: Mapping[str, object]
        The dictionary that contains the assay information.

    Return
    ------
    int
        The database identifier of the new assay.
    '''
    query_str = '''
        INSERT INTO assay
         (name, name_shortcut, active, ngc_name,
          adapter, adapter1, adapter2, singleIndex, doubleIndex, uniqueDI)
         VALUES (?,?,?,?,?,?,?,?,?,?)
    '''
    data = (assay_data['assay'], assay_data['name_shortcut'],
            assay_data['active'], assay_data['ngc_name'],
            assay_data['adapter'], assay_data['adapter1'], assay_data['adapter2'],
            bool(assay_data['singleIndex']),
            bool(assay_data['doubleIndex']),
            bool(assay_data['uniqueDI']))
    db_conn.execute(query_str, data)

    assay_info = query.get_assay_by_name(db_conn, assay_data['assay'])
    __insert_indexes(db_conn, assay_info['id'], assay_data)


def insert_word(db_conn, word_name: str) -> None:
    '''
    Insert the word data into the database.

    Important: the method does not commit the operation. You must do it by yourself.

    Arguments
    ---------
    db_conn
        The database connection object.
    words_name: str
        The pronounceable word to be inserted.

    '''
    query_str = '''
        INSERT INTO word
         (name, used)
         VALUES (?,?)
    '''
    data = (word_name, 0)
    db_conn.execute(query_str, data)


def delete_samplesheet(db_conn, fullname: str) -> None:
    '''
    Remove from the database the samplesheet that has the given full name.

    Important: the method does not commit the operation. You must do it by yourself.

    Arguments
    ---------
    db_conn
        The database connection object.
    fullname: str
        The samplesheet full name.
    '''
    # Retrieve the sample sheet object.
    samplesheet = query.get_samplesheet_by_fullname(db_conn, fullname)
    if samplesheet:
        db_conn.execute('DELETE FROM ngc_library WHERE sample_id IN (SELECT id FROM sample WHERE samplesheet_id = ?)', (samplesheet['id'],))
        db_conn.execute('DELETE FROM sample WHERE samplesheet_id = ?', (samplesheet['id'],))
        db_conn.execute('DELETE FROM samplesheet WHERE id = ?', (samplesheet['id'],))


def insert_user(db_conn, username: str, password: str) -> None:
    '''
    Insert a new user into the database.

    Important: the method does not commit the operation. You must do it by yourself.

    Arguments
    ---------
    db_conn
        The database connection object.
    username: str
        The new user name.
    password: str
        The password of the new user.
    '''
    insert_str = 'INSERT INTO user (username, password) VALUES (?, ?)'
    db_conn.execute(insert_str, (username, password))


def insert_ngc_library(db_conn, library: str, library_number: int, sample_id: int) -> None:
    '''
    Insert a new user into the database.

    Important: the method does not commit the operation. You must do it by yourself.

    Arguments
    ---------
    db_conn
        The database connection object.
    library: str
        The library name.
    library_number: str
        The library number.
    sample_id: str
        The sample database identifier.
    '''
    insert_str = '''
        INSERT INTO ngc_library (library_type, library_number, sample_id)
        VALUES (?, ?, ?)
    '''
    db_conn.execute(insert_str, (library, library_number, sample_id))


#########################################################################################
# Private functions section                                                             #
#########################################################################################
def __create_sample_uid(db_conn, samplesheet_info: Mapping[str, object],
                        sample_position: int) -> str:
    '''
    Creates a unique sample id identifier.

    Arguments
    ---------
    db_conn
        The database connection object.
    samplesheet_info: Mapping[str,object]
        A dictionary that contains the sample sheet attributes.
    sample_position: int
        The position of the sample in the list.

    Return
    ------
    str
        The sample unique identifier. It is a composition of
        sample sheet flowcell, assay name and, sample position.
    '''
    flowcell = samplesheet_info['flowcell']
    assay_shortcut = query.get_assay_by_name(db_conn, samplesheet_info['assay'])['name_shortcut']
    number = str(sample_position)
    number = '0' * (3 - len(number)) + number
    return '-'.join([flowcell, assay_shortcut, number])


def __insert_indexes(db_conn, assay_id: int, assay: Mapping[str, object]) -> None:
    '''
    Insert the indexes of the given assay.

    Arguments
    ---------
    db_conn
        The database connection object.
    assay_id:int
        The database identifier of the new assay.
    assay:
        The assay data dictionary.
    '''
    if assay['singleIndex']:
        insert_str = 'INSERT INTO index1 (name, bases, assay_id) VALUES (?,?,?)'
        nelements = len(assay['singleIndex']['indexes']['name'])
        data = list(zip(*(assay['singleIndex']['indexes']['name'],
                          assay['singleIndex']['indexes']['bases'],
                          [assay_id for _ in range(nelements)])))
        __insert_many(db_conn, insert_str, data)

    elif assay['doubleIndex']:
        insert_str = 'INSERT INTO index1 (name, bases, assay_id) VALUES (?,?,?)'
        nelements = len(assay['doubleIndex']['i7indexes']['name'])
        data = list(zip(*(assay['doubleIndex']['i7indexes']['name'],
                          assay['doubleIndex']['i7indexes']['bases'],
                          [assay_id for _ in range(nelements)])))
        __insert_many(db_conn, insert_str, data)

        insert_str = 'INSERT INTO index2 (name, novabases, nextbases, assay_id) VALUES (?,?,?,?)'
        nelements = len(assay['doubleIndex']['i5indexes']['name'])
        data = list(zip(*(assay['doubleIndex']['i5indexes']['name'],
                          assay['doubleIndex']['i5indexes']['novabases'],
                          assay['doubleIndex']['i5indexes']['nextbases'],
                          [assay_id for _ in range(nelements)])))
        __insert_many(db_conn, insert_str, data)

    elif assay['uniqueDI']:
        insert_str = '''
            INSERT INTO indexDouble (name, i7bases, i5novabases, i5nextbases, assay_id)
             VALUES (?,?,?,?,?)
        '''
        nelements = len(assay['uniqueDI']['indexes']['name'])
        data = list(zip(*(assay['uniqueDI']['indexes']['name'],
                          assay['uniqueDI']['indexes']['i7bases'],
                          assay['uniqueDI']['indexes']['i5novabases'],
                          assay['uniqueDI']['indexes']['i5nextbases'],
                          [assay_id for _ in range(nelements)])))
        __insert_many(db_conn, insert_str, data)


def __insert_many(db_conn, insert_str, index_data) -> None:
    '''
    Execute the insert of muliple elements.

    Arguments
    ---------
    db_conn
        The database connection object.
    insert_str: str
        The insert SQL command that should be executed.
    index_data
        The set of data to be inserted into the database.
    '''
    db_conn.executemany(insert_str, index_data)
