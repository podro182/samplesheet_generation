# -*- coding: utf-8 -*-

#########################################################################################
# Copyright (C) 2020 GENOMIC MEDICINE - RIGSHOSPITALET                                  #
#########################################################################################

'''
Sample sheet generation module.

This module groups the implementation of the samplesheet_generation application.

Copyright (C) 2020 GENOMIC MEDICINE - RIGSHOSPITALET

Authors : Bioinformatics team of the Genomic Medicine Department @ RIGSHOSPITALET
- Savvas Kinalis <savvas.kinalis@regionh.dk>
- Mathias Husted Torp <mathias.husted.torp@regionh.dk>
- Filipe Garrett Vieira <filipe.garrett.vieira@regionh.dk>
- Christian Baudet <christian.baudet.01@regionh.dk>
'''

#########################################################################################
# Import section                                                                        #
#########################################################################################
import os
import sys
import shutil
import subprocess

from pathlib import Path
from flask import Flask, redirect, url_for
from samplesheet_generation import db, auth, project, samplesheet, constants

#########################################################################################
# Functions section                                                                     #
#########################################################################################

def create_app():
    '''
    Creates the samplesheet_generation application.

    This function loads configuration and wire up everything.

    Argument
    --------
    test_config
        Default None. A dictionary containing the test configuration.

    Return
    ------
    The reference for the samplesheet_generation application object.
    '''

    app = Flask(__name__, instance_relative_config=True)

    # Updating the configuration with the base dir
    __update_config(app)

    # Initializing the database module
    db.init_app(app)

    # Wiring up the blueprints
    app.register_blueprint(auth.blueprint)
    app.register_blueprint(project.blueprint)
    app.register_blueprint(samplesheet.blueprint)

    # When the user access the URL root, redirect him/her to the login page.
    @app.route('/')
    def _():
        return redirect(url_for('auth.login'), code=302)

    # Done, returning the configuration
    return app


def __update_config(app):
    '''
    Updates the configuration based on the environment variables.

    Argument
    --------
    app
        The application object.
    '''
    app.logger.info('Updating system configuration')

    # Loading the configuration of the application
    app.config.from_envvar(constants.APP_CONFIG_FILE_ENV_VARNAME, silent=True)

    # Data directory
    data_dir = __get_env_var(constants.DATA_DIR_ENV_VARNAME)
    if data_dir:
        # Database file
        dbdir_path = Path(data_dir).joinpath(constants.DB_DIRNAME)
        db_file = dbdir_path.joinpath(constants.DB_FILENAME)
        app.config[constants.CONFIG_DB_FILE] = db_file.as_posix()

        # Sheets directory
        sheets_dir = Path(data_dir).joinpath(constants.SHEETS_DIRNAME)
        app.config[constants.CONFIG_SHEETS_DIR] = sheets_dir.as_posix()


def __get_env_var(varname: str, raise_exception: bool = False) -> str:
    '''
    Returns the value of the environment variable that has the given name.

    Argument
    --------
    varname: str
        The name of the desired variable.
    raise_exception: bool
        If True, raise an exception if the variable is not set.

    Return
    ------
    str
        The value of the desired variable.


    Raise
    -----
    RuntimeError
        When the environment variable is not defined and raise_exception is True.
    '''
    value = os.environ.get(varname)
    if raise_exception and not value:
        raise RuntimeError(f'The environment variable {varname} is not defined.')
    return value


#########################################################################################
# Main program section                                                                  #
#########################################################################################
application = create_app() #pylint:disable=invalid-name
if __name__ == "__main__":
    # application.run(debug=True, use_debugger=False, use_reloader=False, passthrough_errors=True)
    application.run()
