#!/bin/bash

NETWORK_NAME="gm_docker_dev_network"
SUBNET="192.169.0"

aux=`docker network ls | grep ${NETWORK_NAME}`

if [ -z "$aux" ]; then
    docker network create -d bridge --subnet ${SUBNET}.0/24 --gateway ${SUBNET}.1 ${NETWORK_NAME}
else
    echo "${NETWORK_NAME} is already available."
    docker network inspect ${NETWORK_NAME}
fi
