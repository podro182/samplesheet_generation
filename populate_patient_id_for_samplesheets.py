#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#########################################################################################
# Copyright (C) 2020 GENOMIC MEDICINE - RIGSHOSPITALET                                  #
#########################################################################################
'''
Script for updating the assay table on the database.

Copyright (C) 2020 GENOMIC MEDICINE - RIGSHOSPITALET

Authors : Bioinformatics team of the Genomic Medicine Department @ RIGSHOSPITALET
- Savvas Kinalis <savvas.kinalis@regionh.dk>
- Mathias Husted Torp <mathias.husted.torp@regionh.dk>
- Filipe Garrett Vieira <filipe.garrett.vieira@regionh.dk>
- Christian Baudet <christian.baudet.01@regionh.dk>
'''

#########################################################################################
# Import section                                                                        #
#########################################################################################
import os
import sys

from pathlib import Path
from argparse import ArgumentParser, Namespace
from samplesheet_generation import db, constants, samplesheet
from samplesheet_generation.db import query, insert_update

#########################################################################################
# Section : Functions
#########################################################################################
# Parses the command line arguments
def parse_arguments() -> Namespace:
    '''
    Parses the command line arguments.

    This function parses the command line arguments.

    Return
    ------
    Namespace
        A Namesapace object containing the parsed elements.
    '''
    # Configuring the argument parser
    parser = ArgumentParser(description='SSG: Populate patient IDs for specific samplesheets')
    parser.add_argument('samplesheet_ids',
                        type=str, nargs='+',
                        help='Samplesheets that require patient ids.')
    parser.add_argument('-d',
                        dest='directory',
                        required=True,
                        action='store',
                        help='Path for the directory where the SSG database file is located.')

    if len(sys.argv) == 1:
        parser.print_help(sys.stderr)
        sys.exit(1)
    return parser.parse_args()


#########################################################################################
def main() -> None:
    '''
    Main function that coordinates the work.
    '''
    # Reading command line arguments
    args = parse_arguments()

    # Getting the base directory (directory where this script is located)
    base_dir = Path(os.path.realpath(__file__)).parent

    # Creating the path for the database file
    db_path = Path(args.directory).joinpath(constants.DB_FILENAME).absolute()

    # Getting a database connection
    db_conn = db.create_database_connection(raise_exception=True, db_path=db_path)

    for samplesheet_id in args.samplesheet_ids:
        print(samplesheet_id)
        # Find all samples that require a patient ID
        samples = query.get_samples_by_samplesheet_id(db_conn, samplesheet_id)

        for sample in samples:
            # Create patient id if it is required.
            project_info = query.get_project_by_id(db_conn, sample['project_id'])
            has_glass_no = project_info['has_glass_no']
            if has_glass_no:
                print(sample['id'])
                patient_id = samplesheet.__get_or_generate_patient_id(db_conn, sample['sample_number'])
                insert_update.update_sample_with_patient_id(db_conn, sample['id'], patient_id)

    # Commit changes
    db_conn.commit()


#########################################################################################
if __name__ == "__main__":
    main()
