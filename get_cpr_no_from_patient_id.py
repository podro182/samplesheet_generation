#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#########################################################################################
# Copyright (C) 2020 GENOMIC MEDICINE - RIGSHOSPITALET                                  #
#########################################################################################
'''
Script for updating the assay table on the database.

Copyright (C) 2020 GENOMIC MEDICINE - RIGSHOSPITALET

Authors : Bioinformatics team of the Genomic Medicine Department @ RIGSHOSPITALET
- Savvas Kinalis <savvas.kinalis@regionh.dk>
- Mathias Husted Torp <mathias.husted.torp@regionh.dk>
- Filipe Garrett Vieira <filipe.garrett.vieira@regionh.dk>
- Christian Baudet <christian.baudet.01@regionh.dk>
'''

#########################################################################################
# Import section                                                                        #
#########################################################################################
import os
import sys

from pathlib import Path
from argparse import ArgumentParser, Namespace
from samplesheet_generation import db, constants
from samplesheet_generation.db import query

#########################################################################################
# Section : Functions
#########################################################################################
# Parses the command line arguments
def parse_arguments() -> Namespace:
    '''
    Parses the command line arguments.

    This function parses the command line arguments.

    Return
    ------
    Namespace
        A Namesapace object containing the parsed elements.
    '''
    # Configuring the argument parser
    parser = ArgumentParser(description='Get the cpr no from patient id.')
    parser.add_argument('patient_ids',
                        type=str, nargs='+',
                        help='Patient ids to be translated to CPR numbers.')
    parser.add_argument('-d',
                        dest='directory',
                        required=True,
                        action='store',
                        help='Path for the directory where the SSG database file is located.')
    if len(sys.argv) == 1:
        parser.print_help(sys.stderr)
        sys.exit(1)
    return parser.parse_args()


# Fetch the cpr number
def __get_cpr(db_conn, patient_id):
    patient_info = query.get_patient_by_name(db_conn, patient_id)
    if patient_info:
        cpr = str(patient_info['cpr_no'])[0:6] + '-' + str(patient_info['cpr_no'])[6:10]
    else:
        cpr = ''
    return cpr


#########################################################################################
def main() -> None:
    '''
    Main function.
    '''
    # Reading command line arguments
    args = parse_arguments()

    # Getting the base directory (directory where this script is located)
    base_dir = Path(os.path.realpath(__file__)).parent

    # Creating the path for the database file
    db_path = Path(args.directory).joinpath(constants.DB_FILENAME).absolute()

    # Getting a database connection
    db_conn = db.create_database_connection(raise_exception=True, db_path=db_path)

    # Fetching
    cpr_no = dict()
    for patient_name in args.patient_ids:
        cpr_no[patient_name] = __get_cpr(db_conn, patient_name)

    # Printing
    print(cpr_no)


#########################################################################################
if __name__ == "__main__":
    main()
