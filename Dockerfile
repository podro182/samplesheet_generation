#########################################################################################
# Copyright (C) 2020 GENOMIC MEDICINE - RIGSHOSPITALET                                  #
#########################################################################################

# Trying to use a minimal image
FROM python:3.7.6-alpine3.11

# This will be the instalation place of the samplesheet generator.
WORKDIR /usr/local/samplesheet_generator

# This argument is received by the docker-compose configuration file
ARG dependencies

# Install packages that are necessary for installing cx-Oracle
RUN apk add build-base libaio libc6-compat

# We copy the oracle client to the work dir folder
COPY instantclient-basic-linux.x64-19.6.0.0.0dbru.zip .

# Unzip the oracle client, put it in the right place and create the necessary symlinks
RUN unzip instantclient-basic-linux.x64-19.6.0.0.0dbru.zip -d /usr/lib && \
    ln -s /usr/lib/instantclient_19_6/*.so /usr/lib/ && \
    ln -s /usr/lib/libnsl.so.2 /usr/lib/libnsl.so.1 && \
    ln -s /lib/libc.so.6 /usr/lib/libresolv.so.2 && \
    ln -s /lib64/ld-linux-x86-64.so.2 /usr/lib/ld-linux-x86-64.so.2

# We copy the requirements.txt to the work dir folder
COPY "$dependencies" .

# Upgrading pip and installing the dependencies
RUN python -m pip install --upgrade pip && pip3 install -r "$dependencies"

# We copy all the project to the work dir folder
COPY . .

# Remove the unnecessary oracle client zip
RUN rm -f instantclient-basic-linux.x64-19.6.0.0.0dbru.zip

# Giving execution permissions for the start server script
RUN chmod +x start_server_docker.sh

# Defining the entrypoint of the machine
ENTRYPOINT [ "./start_server_docker.sh" ]
