#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#########################################################################################
# Copyright (C) 2020 GENOMIC MEDICINE - RIGSHOSPITALET                                  #
#########################################################################################
'''
Auxiliary script that forces the regeneration of a sample sheet.

Copyright (C) 2020 GENOMIC MEDICINE - RIGSHOSPITALET

Authors : Bioinformatics team of the Genomic Medicine Department @ RIGSHOSPITALET
- Savvas Kinalis <savvas.kinalis@regionh.dk>
- Mathias Husted Torp <mathias.husted.torp@regionh.dk>
- Filipe Garrett Vieira <filipe.garrett.vieira@regionh.dk>
- Christian Baudet <christian.baudet.01@regionh.dk>
'''

#########################################################################################
# Import section                                                                        #
#########################################################################################
import sys

from pathlib import Path
from argparse import ArgumentParser, Namespace

from samplesheet_generation import db, csv, constants
from samplesheet_generation.db import query

#########################################################################################
# Section : Functions
#########################################################################################
# Parses the command line arguments
def parse_arguments() -> Namespace:
    '''
    Parses the command line arguments.

    This function parses the command line arguments.

    Return
    ------
    Namespace
        A Namesapace object containing the parsed elements.
    '''
    # Configuring the argument parser
    parser = ArgumentParser(description='SSG: Regenerate sample sheet file.')
    parser.add_argument('-d',
                        dest='db_dir',
                        required=True,
                        action='store',
                        help='Path for the directory where the SSG database file is located.')
    parser.add_argument('-s',
                        dest='sheets_dir',
                        required=True,
                        action='store',
                        help='Path for the sheets directory (where the run directories with the samplesheets are created).') #pylint:disable=line-too-long
    parser.add_argument('-r',
                        dest='runname',
                        required=True,
                        action='store',
                        help='The name of the sequencing run (name that is registered in the database)') #pylint:disable=line-too-long
    if len(sys.argv) == 1:
        parser.print_help(sys.stderr)
        sys.exit(1)
    return parser.parse_args()


#########################################################################################
def main() -> None:
    '''
    Main function that coordinates the work.
    '''
    # Reading command line arguments
    args = parse_arguments()

    # Creating the path for the database file
    db_path = Path(args.db_dir).joinpath(constants.DB_FILENAME).absolute()

    # Creating the configuration dictionary with the sheets directory.
    config = {constants.CONFIG_SHEETS_DIR : Path(args.sheets_dir).absolute().as_posix()}

    # Getting a database connection
    db_conn = db.create_database_connection(raise_exception=True, db_path=db_path)

    # Recovering the sample sheet
    sample_sheet = query.get_samplesheet_by_fullname(db_conn, args.runname)
    if sample_sheet:
        csv.create_samplesheet_file(db_conn, sample_sheet['id'], config)
    else:
        print(f'Could not find {args.runname} on the database.', file=sys.stderr)

#########################################################################################
if __name__ == "__main__":
    main()
